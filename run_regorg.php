<?php

require_once dirname(__FILE__) . '/main.php';

$last_id = @file_get_contents(dirname(__FILE__).'/run_regorg.txt');
if (!is_numeric($last_id)) { $last_id = 0; }

$orgM = new organizationModel();
$organizations = $orgM->GetOrgListByRegNum($last_id, 400);

foreach ($organizations as $orgnization) {
    $url = "http://staf.multitender.ru/org.php?gzip=0&regNumber={$orgnization['regnum']}";    
    echo $url . "\n";     
    $content = @file_get_contents($url);    
    
    if (!$content) { continue; }
    
    $xml = simplexml_load_string($content);
    var_dump($xml);
    
    $org = array();    
    $org['regnum'] = $orgnization['regnum'];
    $org['shortName'] = reset($xml->nsiOrganization->shortName);
    $org['fullName'] = reset($xml->nsiOrganization->fullName);
    $org['inn'] = reset($xml->nsiOrganization->INN);
    $org['kpp'] = reset($xml->nsiOrganization->KPP);
    $org['ogrn'] = reset($xml->nsiOrganization->OGRN);
    $org['factAddress'] =reset($xml->nsiOrganization->factualAddress->addressLine);
    $org['postAddress'] =reset($xml->nsiOrganization->postalAddress);
    $org['kladrCode'] = reset($xml->nsiOrganization->factualAddress->region->kladrCode);
    $org['region_id'] = reset($xml->nsiOrganization->factualAddress->region->fullName);
    $org['email'] = reset($xml->nsiOrganization->email);
    $org['telephone'] = reset($xml->nsiOrganization->phone);
    $org['fax'] = reset($xml->nsiOrganization->fax);
    $org['contactPerson'] = reset($xml->nsiOrganization->contactPerson->firstName) . " ". reset($xml->nsiOrganization->contactPerson->lastName) ." ". reset($xml->nsiOrganization->contactPerson->middleName);
    
    $orgM->update($orgnization['id'], $org);
    
    file_put_contents(dirname(__FILE__).'/run_regorg.txt', $orgnization['id']);
    
}



