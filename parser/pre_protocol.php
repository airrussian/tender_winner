<?php

class pre_protocolParser extends protocolParser {

    public function pre_parse($content) {
        $return = array();
        $xml = simplexml_load_string($content);

        $return['purchaseNumber'] = reset($xml->protocol->purchaseNumber);

        foreach ($xml->protocol as $protocolXML) {
            $return['protocol'][] = $this->pre_parse_protocol($protocolXML);

            if ($protocolXML->protocolLot) {
                if ($protocolXML->protocolLot->application) {
                    $applicationXML = $protocolXML->protocolLot->application;
                    if (isset($journalNumber[reset($applicationXML->journalNumber)])) {
                        $journalNumber[reset($applicationXML->journalNumber)] = array_merge($journalNumber[reset($applicationXML->journalNumber)], $this->xml2arr($applicationXML));
                    } else {
                        $journalNumber[reset($applicationXML->journalNumber)] = $this->xml2arr($applicationXML);
                    }
                }
                if ($protocolXML->protocolLot->applications) {
                    foreach ($protocolXML->protocolLot->applications->application as $applicationXML) {
                        if (isset($journalNumber[reset($applicationXML->journalNumber)])) {
                            $journalNumber[reset($applicationXML->journalNumber)] = array_merge($journalNumber[reset($applicationXML->journalNumber)], $this->xml2arr($applicationXML));
                        } else {
                            $journalNumber[reset($applicationXML->journalNumber)] = $this->xml2arr($applicationXML);
                        }
                    }
                }
            }

            foreach ($journalNumber as $key => $array) {
                if (isset($array['appParticipant'])) {

                    $org['inn'] = $array['appParticipant']['inn'];
                    $org['kpp'] = isset($array['appParticipant']['kpp']) ? $array['appParticipant']['kpp'] : NULL;
                    $org['postAddress'] = isset($array['appParticipant']['postAddress']) ? $array['appParticipant']['postAddress'] : NULL;
                    $org['name'] = $array['appParticipant']['organizationName'];

                    $return['member'][$key]['organization'] = $org;
                    $return['member'][$key]['rating'] = isset($array['appRating']) ? $array['appRating'] : NULL;
                    $return['member'][$key]['price'] = isset($array['priceOffers']['lastOffer']['price']) ? $array['priceOffers']['lastOffer']['price'] : NULL;
                }
            }
        }

        $xml = NULL;

        return $return;
    }

}
