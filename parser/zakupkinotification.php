<?php

class zakupkinotificationParser extends notificationParser {

    public function parse($content) {

        $column = array(
            'internal_id' => 'internal_id|int|clear_all',
            'name' => 'name|clear_all',
            'purchaseNumber' => 'purchaseNumber|clear_all',
            'pubDate' => 'pubDate|datetime_from_UTC',
            'startDate' => 'startDate|datetime_from_UTC',
            'endDate' => 'endDate|datetime_from_UTC',
            'scoringDate' => 'scoringDate|datetime_from_UTC',
            'biddingDate' => 'biddingDate|datetime_from_UTC',
            'type'      => 'type|clear_all',
            'type_name' =>  'type_name|clear_all',
            'site' => 'site|clear_all',
            'organizer' => 'organizer',
            'lot' => 'lot'
        );


        $result = $this->pre_parse($content);

        $return = $this->item_set_colomn($result, $column);
        return $return;
    }

    public function pre_parse($content) {

        $xml = simplexml_load_string($content);

        $return = array();
        $return['internal_id'] = reset($xml->id);
        $return['name'] = reset($xml->purchaseObjectInfo);
        $return['purchaseNumber'] = reset($xml->purchaseNumber);
        $return['pubDate'] = reset($xml->docPublishDate);
        $return['startDate'] = reset($xml->procedureInfo->collecting->startDate);
        $return['endDate'] = reset($xml->procedureInfo->collecting->endDate);
        $return['scoringDate'] = reset($xml->procedureInfo->scoring->date);
        $return['biddingDate'] = reset($xml->procedureInfo->bidding->date);
        $return['type'] = reset($xml->placingWay->code);
        $return['type_name'] = reset($xml->placingWay->name);
        $return['site'] = reset($xml->ETP->code);

        $return['lot'] = array();
        $return['price'] = 0;

        if (isset($xml->lots)) {
            if (is_array(reset($xml->lots))) {
                $lots = reset($xml->lots);
            } else {            
                $lots = array($xml->lots->lot);
            }        
        } else {
            $lots = array($xml->lot);
        }
        foreach ($lots as $lot) {
            $return['lot'][] = $this->parse_lot($lot);
        }

        $return['organizer'] = $this->parse_organizer($xml->purchaseResponsible);

        return $return;
    }

    public function parse_organizer($customer) {

        $column = array(
            'name' => 'name|clear_all',
            'postAddress' => 'postAddress|clear_all',
            'inn' => 'inn|clear_all',
            'kpp' => 'kpp|clear_all',
            'regnum' => 'regnum|clear_all',
        );

        $return = array();

        $return['name'] = reset($customer->responsibleOrg->fullName);
        $return['postAddress'] = reset($customer->responsibleOrg->postAddress);
        $return['inn'] = reset($customer->responsibleOrg->INN);
        $return['kpp'] = reset($customer->responsibleOrg->KPP);
        $return['regnum'] = reset($customer->responsibleOrg->regNum);

        $return = $this->item_set_colomn($return, $column);

        return $return;
    }

    public function parse_lot($lot) {

        $column = array(
            'price' => 'price|to_price',
            'lotObjectInfo' => 'lotObjectInfo|clear_all',
            'financeSource' => 'financeSource|clear_all',
            'lotNumber' => 'lotNumber|to_int',
            'object' => 'object',
            'customer' => 'customer'
        );

        $return['price'] = reset($lot->maxPrice);
        $return['lotObjectInfo'] = reset($lot->lotObjectInfo);
        $return['financeSource'] = reset($lot->financeSource);
        $return['lotNumber'] = isset($lot->lotNumber) ? reset($lot->lotNumber) : 1;

        // Может быть много заказчиков для 
        $return['customer'] = array();
        if (is_array(reset($lot->customerRequirements))) {
            $customerRequirements = reset($lot->customerRequirements);
        } else {            
            $customerRequirements = array($lot->customerRequirements->customerRequirement);
        }
        foreach ($customerRequirements as $customerRequirement) {
            $return['customer'][] = $this->parse_customerRequirement($customerRequirement);
        }

        $return['object'] = array();       
        
        if (is_array(reset($lot->purchaseObjects))) {
            $purchaseObjects = reset($lot->purchaseObjects);
        } else {
            $purchaseObjects = array($lot->purchaseObjects->purchaseObject);
        }        
                
        foreach ($purchaseObjects as $purchaseObject) {
            $return['object'][] = $this->parse_object($purchaseObject);
        }

        $return = $this->item_set_colomn($return, $column);

        return $return;
    }

    public function parse_object($object) {

        $column = array(
            'OKPD' => 'OKPD|clear_all',
            'name' => 'name|clear_all',
            'sum'      => 'sum|clear_all|to_price'
        );

        $return = array();

        $return['OKPD'] = reset($object->OKPD->code);
        $return['name'] = reset($object->name);
        $return['sum']  = reset($object->sum);

        $return = $this->item_set_colomn($return, $column);

        return $return;
    }

    public function parse_customerRequirement($customerRequirement) {

        $column = array(
            'regnum' => 'regnum|clear_all',
            'fullname' => 'name|clear_all',
            'applicationGuarantee' => 'applicationGuarantee|to_price',
            'contractGuarantee' => 'contractGuarantee|to_price'
        );

        $return['regnum'] = reset($customerRequirement->customer->regNum);
        $return['fullname'] = reset($customerRequirement->customer->fullName);
        $return['applicationGuarantee'] = reset($customerRequirement->applicationGuarantee->amount);
        $return['contractGuarantee'] = reset($customerRequirement->contractGuarantee->amount);

        $return = $this->item_set_colomn($return, $column);

        return $return;
    }

}
