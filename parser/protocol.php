<?php

class protocolParser extends baseParser {

    //private $url = "http://staf.multitender.ru/protocol.php?format=json&regNumber=#purchaseNumber#";
    private $url = "http://staf.multitender.ru/protocol.php?regNumber=#purchaseNumber#";

    public function parse($purchaseNumber) {

        $this->url = str_replace("#purchaseNumber#", $purchaseNumber, $this->url);
        
        $protocol = new protocolModel();
        $content = $protocol->getByNumber($purchaseNumber);
        
/*        $content = file_get_contents($this->url); */
        $arr = $this->pre_parse($content);

        foreach ($arr as $key => &$value) {
            if ($key == 'protocol') {
                foreach ($value as &$protocol) {
                    $protocol['protocolDate-SRC'] = $protocol['protocolDate'];
                    $protocol['protocolDate'] = date("Y-m-d H:i:s", strtotime($protocol['protocolDate']));
                    $protocol['signDate-SRC'] = $protocol['signDate'];
                    $protocol['signDate'] = date("Y-m-d H:i:s", strtotime($protocol['signDate']));
                    $protocol['publishDate-SRC'] = $protocol['publishDate'];
                    $protocol['publishDate'] = date("Y-m-d H:i:s", strtotime($protocol['publishDate']));
                }
            }
        }

        return $arr;
    }

    public function pre_parse($content) {
        $return = array();
        $xml = simplexml_load_string($content);
        $return['purchaseNumber'] = reset($xml->protocol->purchaseNumber);

        $n = 0;
        foreach ($xml->protocol as $k => $protocolXML) {
            $n++;
            $return['protocol'][$n] = $this->pre_parse_protocol($protocolXML);

            
            if (isset($protocolXML->protocolLots)) {
                $i = 0;
                foreach ($protocolXML->protocolLots->protocolLot as $protocolLot) {
                    $i++;
                    $return['protocol'][$n]['lot'][$i] = $this->pre_parse_lot($protocolLot);
                }
                $protocolXML = $protocolXML->protocolLots;
            } elseif (isset($protocolXML->protocolLot)) {
                $return['protocol'][$n]['lot'][1] = $this->pre_parse_lot($protocolXML->protocolLot);
            }

            if (isset($protocolXML->protocolLot->applications)) {
                $applicationsXML = $protocolXML->protocolLot->applications;
            }

            if (isset($protocolXML->protocolLot->application)) {
                $applicationsXML = $protocolXML->protocolLot;
            }

            if (isset($applicationsXML)) {
                $j = 0;
                foreach ($applicationsXML->application as $applicationXML) {
                    $j++;
                    $app = $this->pre_parse_application($applicationXML);
                    if (!empty($app)) {
                        $return['protocol'][$n]['application'][$j] = $app;
                    }
                }
                if ($j == 1) {
                    if (isset($return['protocol'][$n]['application'][1]['admitted']) && ($return['protocol'][$n]['application'][1]['admitted'] == 1)) {
                        $return['protocol'][$n]['application'][1]['rating'] = 1;
                    }
                }
            }
        }
       
        return $return;
    }
    
    public function pre_parse_lot($xml) {       
        
        $return = array();
        $return['lotNumber'] = isset($xml->lotNumber) ? reset($xml->lotNumber) : 1;
        if (isset($xml->execution)) {
            $return['execution']['openingDate'] = reset($xml->execution->openingDate);
            $return['execution']['scoringDate'] = reset($xml->execution->scoringDate);
        }
        
        $return['lotObjectInfo'] = reset($xml->lotInfo->lotObjectInfo);
        $return['price'] = reset($xml->lotInfo->maxPrice);
        $return['financeSource'] = reset($xml->lotInfo->financeSource);
        
        if (isset($xml->applications)) {
            $i = 0;
            foreach ($xml->applications->application as $application) {                
                $i++;                
                $return['application'][$i] = $this->pre_parse_application($application);
            }
            if ($i == 1) {                 
                if (isset($return['application'][$i]['admitted']) && ($return['application'][$i]['admitted'] == 1)) { $return['application'][$i]['rating'] = 1; }
            }
        } elseif ($xml->appplication) { 
            $return['application'][1] = $this->pre_parse_application($xml->appplication);
            if (isset($return['application'][1]['admitted']) && ($return['application'][1]['admitted'] == 1)) { $return['application'][1]['rating'] = 1; }
        }                
        
        return $return;
    }
    
    

    public function pre_parse_protocol($xml) {
        $return = array();
        $return['url'] = reset($xml->href);
        $return['protocolNumber'] = reset($xml->protocolNumber);
        $return['protocolDate'] = reset($xml->protocolDate);
        $return['loadDate'] = date("Y-m-d H:i:s");
        $return['signDate'] = reset($xml->signDate);
        $return['publishDate'] = reset($xml->publishDate);
        //$return['other'] = serialize($this->xml2arr($xml));
        return $return;
    }

    public function pre_parse_application($xml) {
        
        $return = array();
        
        if (isset($xml->appParticipant->inn) || isset($xml->appParticipants->appParticipant->inn)) {
            if (isset($xml->appParticipant)) {
                $appParticipant = $xml->appParticipant;
            }
            if (isset($xml->appParticipants->appParticipant)) {
                $appParticipant = $xml->appParticipants->appParticipant;
            }
            $return = $this->pre_parse_org($appParticipant);            
        } 
        
        if (isset($xml->appRating)) {
            $return['rating'] = isset($xml->appRating) ? reset($xml->appRating) : NULL;
        }
        if (isset($xml->admittedInfo->appRating)) {
            $return['rating'] = isset($xml->admittedInfo->appRating) ? reset($xml->admittedInfo->appRating) : NULL;            
        }
        if (isset($xml->admitted)) {
            $return['admitted'] = 1;            
        }
        
        if (isset($xml->journalNumber)) {
            $return['journalNumber'] = abs(crc_p(reset($xml->journalNumber)));
        } 
        
        if (isset($xml->appDate)) {
            $return['appDate'] = reset($xml->appDate);
        }
        
        if (isset($xml->admittedInfo->resultType)) {
            $return['rating'] = reset($xml->admittedInfo->resultType);
            if ($return['rating'] == 'WIN_OFFER') {
                $return['rating'] = 1;
            } else {
                $return['rating'] = 2;
            }
        }
        
        if (isset($xml->admittedInfo->admitted)) {            
            if ($xml->admittedInfo->admitted == 'true') {
                $return['admitted'] = 1;                
            }            
        }
        
        if (isset($xml->price)) {
            $return['price'] = reset($xml->price);
        }
        
        if (isset($xml->priceOffers)) {
            $return['price'] = reset($xml->priceOffers->lastOffer->price);
        }
        

        return $return;
    }

    public function pre_parse_org($xml) {
        $org = array();
        $org['inn'] = reset($xml->inn);
        $org['kpp'] = isset($xml->kpp) ? reset($xml->kpp) : NULL;
        $org['postAddress'] = isset($xml->postAddress) ? reset($xml->postAddress) : NULL;
        $org['name'] = isset($xml->organizationName) ? reset($xml->organizationName) : NULL;
        return $org;
    }

}
