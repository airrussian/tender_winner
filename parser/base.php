<?php

class baseParser {

    public function xml2arr($xml) {
        return json_decode(json_encode($xml), TRUE);
    }

    // регулярные выражения - нужно находить закрывающийся тэг
    final function parse_table($content) {
        $content_dom = str_get_html($content);
        $table = $content_dom->find('table ', 0);
        if (empty($table)) {
            return null;
        }

        //проверка на вложенные теги tbody и thead
        $check = array();
        $i = 0;
        foreach ($table->children() as $ch) {
            if ((strcasecmp($ch->tag, "tbody") == 0) || (strcasecmp($ch->tag, "thead") == 0)) {
                $check[$i] = $ch;
                $i++;
            }
        }
        $check[$i] = $table;

        //$trs = $content_dom->find('tr');

        $return = array();


        $i = 0;
        foreach ($check as $table) {
            foreach ($table->children() as $tr) {
                if (strcasecmp($tr->tag, "tr") == 0) {
                    $j = 0;
                    foreach ($tr->children() as $td) {
                        if ((strcasecmp($td->tag, "td") == 0) || (strcasecmp($td->tag, "th") == 0)) {
                            $return[$i][$j] = trim($td->innertext);
                            $j++;
                        }
                    }
                    $i++;
                }
            }
        }

        $content_dom->__destruct();

        return $return;
    }

    final function text_to_int($str, $delemiter = false, $charfloar = false) {
        if ($delemiter == ",") {
            $str = preg_replace("#\,\s*#si", "", $str);
        }
        if ($delemiter == ".") {
            $str = preg_replace("#\.\s*#si", "", $str);
        }
        if ($delemiter == " ") {
            $str = preg_replace("#\s*#si", "", $str);
        }

        return (int) $str;
    }

    /**
     * Удаляет тэги, символы.
     * @param string $str
     * @return string
     */
    final function text_clear_all($str) {
        if (is_null($str)) {
            return null;
        }

        // порядок очень важен (ОГУК Концертно-выставочный центр &lt;Губернский>)
        $str = $this->text_clear_enteries($str);
        $str = $this->text_strip_tags($str);
        return $str;
    }

    /**
     * Очишает строку от различных HTML символов.
     * Требует постоянной доработки по ходу дела.
     * Предполагатся UTF-8.
     * $convert = array('&some;'=>' ',);
     * @param string $str
     * @param array $convert_add дополнительные для конвертации
     * @return string
     */
    final function text_clear_enteries($str, $convert_add = null) {
        $convert = array(
            '&nbsp;' => ' ',
            ' ' => ' ',
            '&shy;' => '',
            '&#8470;' => '№',
            '&ndash;' => '-',
            '–' => '-',
            '&mdash;' => '-',
            '—' => '-',
            '--' => '-',
            '−' => '-', // minus
            '&lt;' => '«',
            '&gt;' => '»',
        );

        if (is_array($convert_add)) {
            $convert += $convert_add;
        }

        $str = str_replace(array_keys($convert), array_values($convert), $str);
        $str = html_entity_decode($str, ENT_QUOTES, 'UTF-8');
        return $this->text_strip_space($str);
    }

    /**
     * Удаляет все тэги, весьма топорно.
     * @param string $str
     * @return string
     */
    final function text_strip_tags($str) {
        $str = preg_replace(array('#<script[^>/]*>.*</script>#siU',
            '#<style[^>/]*>.*</style>#siU',
            '#<!--.*-->#siU'), ' ', $str);
        // тэги-разделители
        $str = preg_replace('/<(br|p|tr|td)/i', ' $0', $str);
        $str = strip_tags($str);
        return $this->text_strip_space($str);
    }

    /**
     * Очишает строку от повторяющихся пробельных символов.
     * @param string $str
     * @return string
     */
    final function text_strip_space($str) {
        $str = preg_replace('/\s+/u', ' ', $str);
        return trim($str);
    }

    /**
     * to price
     * @param string $str
     * @return int
     */
    final function text_to_price($str) {        
        $price = preg_replace("/[^\d.,]/", "", $str);        
        $price = (int) $str * 100;
        return is_numeric($price) ? $price : null;
    }

    final function text_datetime_from_UTC($dateUTC) {
        if (is_null($dateUTC) || $dateUTC == false) {
            return NULL;
        }
        return date("Y-m-d H:i:s", strtotime($dateUTC . ' UTC'));
    }

    public function item_set_colomn($item, $column) {
        $return = array();
        foreach ($column as $key => $functions) {
            $functions = explode("|", $functions);
            $new_key = $functions[0];
            unset($functions[0]);
            $return[$new_key] = $item[$key];
            $return[$key . "-SRC"] = $item[$key];            
            foreach ($functions as $function) {
                $function = "text_" . $function;
                if (method_exists($this, $function)) {
                    $return[$new_key] = $this->$function($return[$new_key]);
                }
            }
        }
        return $return;
    }

}
