<?php

class zk_protocolParser extends protocolParser {

    public function pre_parse($content) {
        $return = array();
        $xml = simplexml_load_string($content);

        $return['purchaseNumber'] = reset($xml->protocol->purchaseNumber);

        foreach ($xml->protocol as $protocolXML) {
            $return['protocol'][] = $this->pre_parse_protocol($protocolXML);
            if ($protocolXML->protocolLot) {
                if ($protocolXML->protocolLot->application) {
                    $applicationXML = $protocolXML->protocolLot->application;
                    if (isset($journalNumber[reset($applicationXML->journalNumber)])) {
                        $journalNumber[reset($applicationXML->journalNumber)] = array_merge($journalNumber[reset($applicationXML->journalNumber)], $this->xml2arr($applicationXML));
                    } else {
                        $journalNumber[reset($applicationXML->journalNumber)] = $this->xml2arr($applicationXML);
                    }
                }
                if ($protocolXML->protocolLot->applications) {
                    foreach ($protocolXML->protocolLot->applications->application as $applicationXML) {
                        if (isset($journalNumber[reset($applicationXML->journalNumber)])) {
                            $journalNumber[reset($applicationXML->journalNumber)] = array_merge($journalNumber[reset($applicationXML->journalNumber)], $this->xml2arr($applicationXML));
                        } else {
                            $journalNumber[reset($applicationXML->journalNumber)] = $this->xml2arr($applicationXML);
                        }
                    }
                }
            }
            
            foreach ($journalNumber as $key => $array) {
                if (isset($array['appParticipant'])) {

                    $org['inn'] = $array['appParticipant']['inn'];
                    $org['kpp'] = isset($array['appParticipant']['kpp']) ? $array['appParticipant']['kpp'] : NULL;
                    $org['postAddress'] = isset($array['appParticipant']['postAddress']) ? $array['appParticipant']['postAddress'] : NULL;
                    $org['name'] = isset($array['appParticipant']['organizationName']) ? $array['appParticipant']['organizationName'] : NULL;

                    $return['member'][$key]['organization'] = $org;
                    if ( isset($array['admittedInfo']['resultType']) ) {
                        if ($array['admittedInfo']['resultType'] == 'WIN_OFFER') {
                            $return['member'][$key]['rating'] = 1;
                        } else {
                            $return['member'][$key]['rating'] = 1+$key;
                        }
                    }
                    $return['member'][$key]['price'] = isset($array['price']) ? $array['price'] : NULL;
                }
            }
        }
        
        $xml = NULL;
        
        return $return;
    }

}
