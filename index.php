<?php

error_reporting(E_ALL);

ini_set('display_errors', 'On');

require_once dirname(__FILE__) . '/main.php';

$controller = isset($_REQUEST['controller']) ? (string) ucfirst($_REQUEST['controller']) : 'Index';
$className = "MT" . $controller . "Controller";

$class = new $className;
$result = $class->execute();

if (!is_null($result)) {
    foreach ($result['headers'] as $header) {
        header($header);
    }

    if (is_array($result['content'])) {
        print_r($result['content']);
    } else {
        echo $result['content'];
    }
}
