<?php

class MTUrlHelper {
    
    public function redirect($url, $type = 301) {        
        header("Location: $url");
        exit();
    }
    
    public function redirectError($message, $type=403) {
        header('HTTP/1.0 400 Bad Request');
        exit($message);
    }
    
}