<?php

header("Content-type: text/html; charset=utf-8");

require_once dirname(__FILE__) . '/main.php';

$num = isset($_REQUEST['num']) ? $_REQUEST['num'] : false;
if (!$num) {
    exit("WHERE IS NUM?");
}

$winner_m = new MTWinnerModel();
$winner = $winner_m->GetByPurchaseNumber($num);

echo json_encode($winner);
/*
$tender = tenderModel::GetNotificationXML($num);

var_dump($tender);*/

//$contract = new MTContractModel();
//echo MTContractModel::Get($num);
