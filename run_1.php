<?php

require_once dirname(__FILE__) . '/main.php';

$last_id = @file_get_contents(dirname(__FILE__).'/last_id_1.txt');
if (!is_numeric($last_id)) { $last_id = 5000000; }

// $data_protocol = new PDO('mysql:host=localhost;dbname=tenders', 'root', 'tk90210', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
$data_protocol = new PDO('sqlite:/home/admin/zakupki/db/protocols.sdb');
$r = $data_protocol->query("SELECT MAX(id) as id, purchaseNumber FROM data WHERE id < $last_id GROUP BY purchaseNumber ORDER BY id DESC LIMIT 10000");
$protocols = $r->fetchAll();

foreach ($protocols as $tender) {
    
    echo "parse = {$tender['purchaseNumber']} \n";
    $loader = new winnerLoader();
    $loader->run($tender['purchaseNumber']);    
    file_put_contents(dirname(__FILE__).'/last_id_1.txt', $tender['id']);
}
