<?php

require_once dirname(__FILE__) . '/main.php';

$date = isset($_REQUEST['date']) ? $_REQUEST['date'] : false;
$dateload = isset($_REQUEST['dateload']) ? $_REQUEST['dateload'] : false;
if (!$date && !$dateload) {
    exit("WHERE IS DATE?");
}

if ($dateload) {
    $date = $dateload;
    $load = "load";
} else {
    $load = "";
}

if (!file_exists(dirname(__FILE__) . '/cache/' . $date . $load . '.xml') || isset($_REQUEST['nocache'])) {
    $winnerModel = new winnerModel();
    if ($load) {
        $winners = $winnerModel->GetByDateLoad($date);
        $wins = array();
        foreach ($winners as $winner) {            
            $wins[$winner['inn']][] = "\t\t<purchaseNumber protocolDate=\"{$winner['protocolDate']}\" publishProtocolDate=\"{$winner['publishDate']}\">" . $winner['purchaseNumber'] . "</purchaseNumber>\n";
            $winners[$winner['inn']][] = $winner;
        }        
        $result = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<winners date=\"$date\">\n";
        foreach ($wins as $inn => $win) {
            $result.= "\t<winner inn=\"{$inn}\">\n";
            foreach ($win as $tender) {
                $result .= $tender;
            }
            $result.= "\t</winner>\n";
        }        
        $result.= "</winners>\n"; 
    } else {
        $winners = $winnerModel->GetByDate($date);
        $result = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<winners date=\"$date\">\n";
        foreach ($winners as $winner) {
            $result.= "\t<winner inn=\"{$winner['inn']}\">\n";
            $purchaseNumbers = explode(",", $winner['tender_ids']);
            foreach ($purchaseNumbers as $purchaseNumber) {
                $result.= "\t\t<purchaseNumber>" . $purchaseNumber . "</purchaseNumber>\n";
            }
            $result.= "\t</winner>\n";
        }
        $result.= "</winners>\n";
    }

    file_put_contents(dirname(__FILE__) . '/cache/' . $date . $load . '.xml', $result);
} else {
    $result = file_get_contents(dirname(__FILE__) . '/cache/' . $date . $load . '.xml');
}

header("Content-type: text/xml; charset=utf-8");
echo $result;

