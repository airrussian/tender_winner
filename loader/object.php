<?php

class objectLoader extends baseLoader {
    
    private $field = array(
        'lot_id',       
        'number',
        'OKPD',
        'name',                  
        'sum'       =>  'maybenull'
    );
    
    
    public function load($data) {
        $object = $this->normalize_field($data, $this->field);
        $objectModel = new objectModel();        
        $item = $objectModel->Load("lot_id = :lot_id AND number = :number ", array('lot_id' => $object['lot_id'], 'number' => $object['number']));
        if (empty($item)) {
            $item = $objectModel->Save($object);
        }        
        
        return $item;                
    }   
    
    
}