<?php

class memberLoader extends baseLoader {

    private $fields = array(
        'tender_id' => 'INT',
        'organization_id' => 'INT',
        'price' => 'maybenull',
        'rating' => 'maybenull'
    );

    public function loader($member) {
        $insert = array();

        foreach ($this->fields as $fieldName => $param) {
            if (!isset($member[$fieldName]) && ($param != 'maybenull')) {
                echo "ERROR, WHERE IS $fieldName";
                return false;
            }
            $insert[$fieldName] = $member[$fieldName];
        }

        $member = new memberModel();
        $member->add($insert);
    }

}
