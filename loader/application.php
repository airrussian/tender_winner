<?php

class applicationLoader extends baseLoader {

    private $fields = array(
        'protocol_id' => 'INT',
        'organization_id' => 'INT',
        'rating' => 'maybenull',
        'admitted'  =>  'maybenull'
    );

    public function loader($application) {
        $insert = array();

        foreach ($this->fields as $fieldName => $param) {
            if (!isset($application[$fieldName]) && ($param != 'maybenull')) {
                echo "ERROR, WHERE IS $fieldName";
                return false;
            }
            $insert[$fieldName] = isset($application[$fieldName]) ? $application[$fieldName] : NULL;
        }                

        $application = new applicationModel();
        $application->add($insert);
    }

}
