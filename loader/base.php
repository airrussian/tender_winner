<?php

class baseLoader {
    public $debug = true;

    public function log($message, $title = "MESSAGE") {
        if ($this->debug) {
            $date = date("d/m/Y H:i:s");
            echo "$title [ $date ] $message \n";
        }
    }

    /**
     * // Нормализация данных
     * @param type $data
     * @param type $field
     */
    public function normalize_field($data, $field) {
        $return = array();

        foreach ($field as $key => $value) {
            if (is_numeric($key)) {
                $field[$value] = 'notnull';
                unset($field[$key]);
            }
        }

        foreach ($field as $key => $maybenull) {
            $return[$key] = isset($data[$key]) ? $data[$key] : NULL;
            if (!isset($return[$key]) || is_null($return[$key]) || ($return[$key] === "")) {
                if ($maybenull != 'maybenull') {
                    $this->log("WHERE IS {$key}", 'ERROR');
                }
                $return[$key] = NULL;
            }
        }
        return $return;
    }

}
