<?php

class winnerLoader extends baseLoader {

    public function run($number) {

        $tender = new tenderModel();
        $tender_id = $tender->GetByNumber($number);
        
        $protocolLoader = new protocolLoader();        
        $org = new organizationModel();
        $apps = new appsLoader();       
        
        $parser = new protocolParser();
        $result = $parser->parse($number);   
        
        /*var_dump($result); 
        exit();*/
        
        foreach ($result['protocol'] as $protocol) {
            $protocol['tender_id'] = $tender_id;
            $protocol['internal_id'] = abs(crc_p($protocol['protocolNumber']));

            
            $protocol_id = $protocolLoader->loader($protocol);
            
            if (isset($protocol['lot'])) {
                $lotLoader = new lotLoader();
                foreach ($protocol['lot'] as $lot) {                    
                    $lot['tender_id'] = $tender_id;
                    $lotL = $lotLoader->load($lot);
                    $lot_id = $lotL['id'];                    

                    if (isset($lot['application'])) {
                        foreach ($lot['application'] as $application) {
                            if (isset($application['inn'])) {
                                                        
                            $application['organization_id'] = $org->GetId(array(
                                'inn' => $application['inn'],
                                'kpp' => $application['kpp'],
                                'name' => $application['name'],
                                'postAddress' => $application['postAddress']
                            ));
                            } else {
                                $application['orgnization_id'] = NULL;
                            }               
                            $application['protocol_id'] = $protocol_id;
                            $application['lot_id'] = $lot_id;                            
                            $apps->load($application);
                        }
                    }
                }
            }

            if (isset($protocol['application'])) {
                foreach ($protocol['application'] as $application) {    
                    if (isset($application['inn'])) {
                        $application['organization_id'] = $org->GetId(array(
                            'inn' => $application['inn'],
                            'kpp' => $application['kpp'],
                            'name' => $application['name'],
                            'postAddress' => $application['postAddress']
                        ));
                        $application['protocol_id'] = $protocol_id;
                        $app = new applicationLoader();
                        $app->loader($application);
                    }                    
                }
            }
        }
    }

    public function run2($number) {

        $tender = new tenderModel();
        $tender_id = $tender->GetByNumber($number);

        $parser = new protocolParser();
        $result = $parser->parse($number);

        $a = array();

        foreach ($result['protocol'] as $protocol) {
            $protocol['tender_id'] = $tender_id;
            $protocol['internal_id'] = abs(crc_p($protocol['protocolNumber']));

            $protocolLoader = new protocolLoader();
            $protocol_id = $protocolLoader->loader($protocol);

            if (isset($protocol['application'])) {
                foreach ($protocol['application'] as $application) {
                    $org = new organizationModel();
                    $application['organization_id'] = $org->GetId(array(
                        'inn' => $application['inn'],
                        'kpp' => $application['kpp'],
                        'name' => $application['name'],
                        'postAddress' => $application['postAddress']
                    ));
                    $a[$application['organization_id']] = isset($a[$application['organization_id']]) ? array_merge($a[$application['organization_id']], $application) : $application;
                    $application['protocol_id'] = $protocol_id;
                    $app = new applicationLoader();
                    $app->loader($application);
                }
            }
            $this->insert($tender_id, $a);
        }
    }

    public function insert($tender_id, $applications) {
        echo ($tender_id);
        var_dump($applications);
    }

}
