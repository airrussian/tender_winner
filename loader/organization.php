<?php

class organizationLoader extends baseLoader {
    
    private $update = false;

    private $field = array(
        'inn'           => 'maybenull',
        'regnum'        => 'maybenull',
        'name'          => 'maybenull',
        'kpp'           => 'maybenull',
        'postAddress'   => 'maybenull',
    );
    
    public function __construct($update = false) {
        $this->update = is_bool($update) ? $update : false;
    }

    public function load($organization) {                        

        $organization = $this->normalize_field($organization, $this->field);
        
        if (is_null($organization['inn']) && is_null($organization['regnum'])) {
            $this->log("not field INN and REGNUM for organization", 'ERROR');
            return false;
        } else {
            $where = "regnum LIKE :regnum OR inn LIKE :inn";
            $value = array('regnum' => $organization['regnum'], 'inn' => $organization['inn']);
        }

        if (is_null($organization['inn']) && !is_null($organization['regnum'])) {
            $where = "regnum LIKE :regnum";
            $value = array('regnum' => $organization['regnum']);
        }
        if (is_null($organization['regnum']) && !is_null($organization['inn'])) {
            $where = "regnum LIKE :inn";
            $value = array('inn' => $organization['inn']);
        }
                
        $org = new organizationModel();                        
        $orgDB = $org->Load($where, $value); 
        
        if (empty($orgDB)) {
            $orgDB = $org->Save($organization);
        } 
        return $orgDB['id'];
        
    }
    
}
