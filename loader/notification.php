<?php

class notificationLoader extends baseLoader {

    private $type_id;
    private $region_id;
    private $site_id = 1;
    private $parse_id = 1;
    private $region;
    private $type;
    
    private $update_item = true;
    
    private $field = array(
        'purchaseNumber'=>  'string',
        'name'          =>  'string',
        'pubDate'       =>  'datetime',  
        'startDate'     =>  'datetime|maybenull',
        'scoringDate'   =>  'datetime|maybenull',
        'biddingDate'   =>  'datetime|maybenull',
        'endDate'       =>  'datetime|maybenull',
        'act_id'        =>  'int',
        'type_id'       =>  'int',
        'site_id'       =>  'int',
        'internal_id'   =>  'int',
        'region_id'     =>  'int',
        'organizer_id'  =>  'int'
    );
    
    
    public function getList($from_id = 0, $count = 100) {
        $db = new PDO('sqlite:/home/admin/zakupki/db/notifications.sdb');
        $r = $db->query("SELECT * FROM notifications WHERE id>$from_id ORDER BY id ASC LIMIT $count");
        $notifications = $r->fetchAll();
        return $notifications;
    }
    
    public function getItem($purchaseNumber) {
        $db = new PDO('sqlite:/home/admin/zakupki/db/notifications.sdb');
        $r = $db->query("SELECT * FROM notifications WHERE purchaseNumber LIKE '$purchaseNumber'");
        $notifications = $r->fetchAll();
        return $notifications;
    }

    /**
     * Нужно выяснить все возможности PHP5.3 может ли он наследованит __SET 
     * @param type $array
     */
    public function set($array) {
        foreach ($array as $propertyName => $propertyValue) {
            if (property_exists($this, $propertyName)) {
                $this->$propertyName = $propertyValue;
            }
        }
    }

    public function getContent($purchaseNumber) {
        if (!$purchaseNumber) {
            $this->log("bad " . $purchaseNumber);
            exit();
        }

        $tender = new tenderModel();
        $content = $tender->GetNotificationXML($purchaseNumber);

        if (empty($content)) {
            $this->log("empty notification [ $purchaseNumber ] ");
        }

        return $content;
    }

    public function load($data) {
        
        $organization = new organizationLoader();
        
        if (isset($data['organizer'])) {
            $data['organizer_id'] = $organization->load($data['organizer']);            
        }                
          
        $data['site_id'] = $this->site_id;
        if (isset($data['site'])) {
            $site = new siteModel();
            $site_id = $site->GetId($data['site']);            
            if ($site_id != false) $data['site_id'] = $site_id;
        } 
              
        
        $data['act_id'] = 1;
        
        if (isset($data['type'])) {
            $type = new typeModel();
            $t = $type->dict($data['type'], $data['type_name'], $this->parse_id);            
            if (!is_null($t['type_id'])) {
                $data['type_id'] = $t['type_id'];
            }
        }
                
                
        if ($this->region) {            
            $region = new regionModel();
            $r = $region->dict($this->region, $this->parse_id);            
            if (!is_null($r['region_id'])) {
                $this->region_id = $r['region_id'];                
            }
        }        
        
        $data['region_id'] = $this->region_id;
        
        $tender = $this->normalize_field($data, $this->field);                
        
        if (isset($tender['purchaseNumber'])) {
            $where = 'purchaseNumber LIKE :purchaseNumber';
            $value = array('purchaseNumber' => $tender['purchaseNumber']);
        } elseif (isset($tender['internal_id']) && (isset($tender['site_id']))) {
            $where = 'internal_id = :internal_id AND site_id = :site_id';
            $value = array('internal_id' => $tender['internal_id'], 'site_id' => $tender['site_id']);
        }
        $tenderModel = new tenderModel();
        $item = $tenderModel->Load($where, $value);         
        if (empty($item)) {
            $item = $tenderModel->Save($tender);
        } else {
            if ($this->update_item) {
                $item = array_merge($item, $tender);                
                $item = $tenderModel->Save($item);
            }
        }
        
        $lotLoader = new lotLoader();
        if (isset($data['lot']) && !empty($data['lot'])) {            
            foreach ($data['lot'] as $lot) {
                $lot['tender_id'] = $item['id'];                
                $lotLoader->load($lot);
            }
        }       
    }


    private function GetETPId($etp_code) {
        $site = new siteModel();
        $site_id = $site->GetId($etp_code);
        return $site_id;
    }

    private function GetRegionID() {
        $region_m = new regionModel();

        $region_dict = $region_m->dict($region_name, $parse_id);
        $region_id = is_null($region_dict['region_id']) ? "NULL" : $region_dict['region_id'];
        echo "region_id = $region_id \n";
    }
    
    private function GetTypeId() {
        echo "type_name = $type_name \n";    
        $type_m = new typeModel();    
        $type_dict = $type_m->dict($type_name, $parse_id);
    }

}
