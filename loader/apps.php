<?php

class appsLoader extends baseLoader {

    private $fields = array(
        'lot_id'                => 'INT',        
        'protocol_id'           => 'INT',
        'journalNumber'         => 'INT',
        'organization_id'       => 'maybenull',
        'admitted'              => 'maybenull',
        'price'                 => 'maybenull',
        'rating'                => 'maybenull',
        'appDate'               => 'maybenull',
    );
    
    public function load($application) {
        
        $application = $this->normalize_field($application, $this->fields);
        
        $apps = new appsModel();
        
        $where = "lot_id = :lot_id AND protocol_id = :protocol_id AND journalNumber = :journalNumber";
        $value = array('lot_id' => $application['lot_id'], 'protocol_id' => $application['protocol_id'], 'journalNumber' => $application['journalNumber']);                        
        
        $appLoader = $apps->Load($where, $value);   
        
        if ($appLoader == false || empty($appLoader)) {
            $appLoader = $apps->Save($application);
        } else {
            foreach ($this->fields as $fieldName => $field) {
                if (is_null($appLoader[$fieldName])) {
                    $appLoader[$fieldName] = $application[$fieldName];
                }
                $appLoader = $apps->Update($appLoader);
            }
        }                
        
        return $appLoader;
    }


}
