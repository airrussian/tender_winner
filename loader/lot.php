<?php

class lotLoader extends baseLoader {

    private $field = array(
        'tender_id',        
        'lotNumber'             =>  'maybenull',
        'lotObjectInfo'         =>  'maybenull',
        'price',
        'financeSource'         =>  'maybenull',
    );

    public function load($data) {
        $lot = $this->normalize_field($data, $this->field);
        $lotModel = new lotModel();        
        $item = $lotModel->Load("tender_id = :tender_id AND lotNumber = :lotNumber", array('tender_id' => $lot['tender_id'], 'lotNumber' => $lot['lotNumber']));        
        if (empty($item)) {
            $item = $lotModel->Save($lot);
        }        
        
        
        if (isset($data['object']) && !empty($data['object'])) {                             
            $objectLoader = new objectLoader();
            foreach ($data['object'] as $number => $object) {
                $object['lot_id'] = $item['id'];
                $object['number'] = $number + 1;                
                $objectLoader->load($object);
            }            
            $objectLoader = NULL;            
        }
                
        if (isset($data['customer']) && !empty($data['customer'])) {
            $customerLoader = new customerLoader();
            $organizationLoader = new organizationLoader();
            foreach ($data['customer'] as $customer) {
                $customer['lot_id'] = $item['id'];                     
                $customer['organization_id'] = $organizationLoader->load($customer);                
                $customerLoader->load($customer);
            }            
            $customerLoader = NULL;
        }
        
        
        return $item;                
    }

}
