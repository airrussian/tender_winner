<?php

class protocolLoader extends baseLoader {
    
    private $fields = array(
        'tender_id'         =>  'INT',
        'internal_id'       =>  'INT',
        'protocolNumber'    =>  'VARCHAR(50)',
        'protocolDate'      =>  'DATETIME',
        'loadDate'          =>  'DATETIME',
        'signDate'          =>  'DATETIME',
        'publishDate'       =>  'DATETIME',
        'url'               =>  'VARCHAR(50)',        
    ); 
    
    
    public function loader($protocol) {
        
        $insert = array();
        
        foreach ($this->fields as $fieldName => $param) {
            if (!isset($protocol[$fieldName]) && ($param!='maybenull')) {
                echo "ERROR, WHERE IS $fieldName";
                return false;
            }
            $insert[$fieldName] = $protocol[$fieldName];
        }
        
        $protocol = new protocolModel();
        return $protocol->add($insert);        
    }
    
}