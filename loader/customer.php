<?php

class customerLoader extends baseLoader {
    
    private $field = array(
        'organization_id',        
        'lot_id',
        'applicationGuarantee'  =>  'maybenull',                  
        'contractGuarantee'   =>  'maybenull'                        
    );
    
    
    public function load($data) {
        $customer = $this->normalize_field($data, $this->field);        
        $customerModel = new customerModel();        
        $item = $customerModel->Load("lot_id = :lot_id AND organization_id = :organization_id", array('lot_id' => $customer['lot_id'], 'organization_id' => $customer['organization_id']));        
        if (empty($item)) {
            $item = $customerModel->Save($customer);
        }                
        return $item;                
    }   
    
    
}