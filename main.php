<?php

$GLOBALS['DB'] = new PDO('mysql:host=localhost;dbname=tenders15', 'root', 'tk90210', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'', PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

require_once '/mnt/ada0s1/home/air/libs/smarty/Smarty.class.php';

function __autoload($class) {
    if (preg_match("/MT(.+)/", $class, $rr)) {
        if (preg_match("/([A-Z].+?)([A-Z]{1}.+)/", $rr[1], $result)) {
            $filename = dirname(__FILE__) . "/" . mb_strtolower($result[2]) . "/" . mb_strtolower($result[1]) . ".php";
            if (file_exists($filename)) {
                require_once $filename;
            } else {
                exit("NOT CLASS [$class] INIT $filename");
            }
        } elseif (preg_match("/([A-Z]{1}[a-z]+)/", $rr[1], $result)) {
            $filename = dirname(__FILE__) . "/" . mb_strtolower($result[1]) . ".php";
            if (file_exists($filename)) {
                require_once $filename;
            } else {
                exit("NOT CLASS [$class] INIT $filename");
            }
        }
    } else {
        if (preg_match("/(.+?)([A-Z]{1}.+)/", $class, $result)) {
            $filename = dirname(__FILE__) . '/' . mb_strtolower($result[2]) . '/' . $result[1] . '.php';
            if (file_exists($filename)) {
                require_once $filename;
            } else {
                exit("NOT CLASS [$class] INIT $filename");
            }
        }
    }
}

function crc_p($in) {
    return s_int(crc32($in));
}

function s_int($in) {
    $int_max = pow(2, 31) - 1;
    if ($in > $int_max) {
        return $in - $int_max * 2 - 2;
    }
    return $in;
}
