<?php

require_once dirname(__FILE__) . '/main.php';

$loader = new notificationLoader();

if (isset($argv[1])) {
    $listNotifications = $loader->getItem($argv[1]);        
} else {
    $id = @file_get_contents(dirname(__FILE__) . '/notification_id.txt');
    if (!is_numeric($id)) {
        $id = 0;
    }    
    $listNotifications = $loader->getList($id, 1500);
}

$parse_id = 1;

foreach ($listNotifications as $notification) {

    $purchaseNumber = $notification['purchaseNumber'];
    $loader->log("getContent [ $purchaseNumber ]");
    $content = $loader->getContent($purchaseNumber);

    // FIX ME: Выясняется название региона из записи в базе Макса
    if (preg_match("~fcs_regions/(.+?)/~si", $notification['fname'], $r)) {
        $region_name = $r[1];
    }            

    $type_name = $notification['placingWay'];

    $loader->set(array('region' => $region_name, 'type' => $type_name));

    $parser = new zakupkinotificationParser();
    $data = $parser->parse($content);
    
    if (!isset($argv[1])) {
        file_put_contents(dirname(__FILE__) . '/notification_id.txt', $notification['id']);
    }
    
    $loader->load($data);
        
}