<winners{if $search.protocol.date} protocoldatefrom="{$search.protocol.date.min}" protocoldateto="{$search.protocol.date.max}"{/if}{if $search.protocol.load} protocolloaddatefrom="{$search.protocol.load.min}" protocolloaddateto="{$search.protocol.load.max}"{/if}>
    {foreach from=$winners key=inn item=winner}
    <winner inn="{$inn}">
        {foreach from=$winner item=item}
            <purchaseNumber{if $item.type_id} type="{$item.type_id}"{/if}{if $item.protocolDate} protocolDate="{$item.protocolDate}"{/if}{if $item.publishDate} publishDate="{$item.publishDate}"{/if}>{$item.purchaseNumber}</purchaseNumber>
        {/foreach}
    </winner>
    {/foreach}
</winners>