<tenders>
    {foreach from=$items key=purchaseNumber item=item}
        <tender purchaseNumber="{$purchaseNumber}">            
            {if $item != NULL}
                <name>{$item.name}</name>
                <price>{$item.price}</price>                
                {if $item.lots|@count > 1} 
                <lots>
                    {foreach from=$item.lots item=lot}
                        <lot>                            
                            <price>{$lot.price}</price>
                            <bankgarant>{$lot.contractGuarantee}</bankgarant>
                        </lot>
                    {/foreach}
                </lots>
                {else}
                <bankgarant>{$item.contractGuarantee}</bankgarant>
                {/if}
            {else}
                not found
            {/if}
        </tender>
    {/foreach}
</tenders>