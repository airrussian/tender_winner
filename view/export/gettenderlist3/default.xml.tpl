<tenders>
    {foreach from=$tenders item=tender}
        <tender purchaseNumber="{$tender.purchaseNumber}">            
            <name>{$tender.name|escape}</name>
            <lots>
                {foreach from=$tender.lots item=lot}
                    <lot>
                        <name>{$lot.lotObjectInfo|escape}</name>
                        <price>{$lot.price/100}</price>
                        <customers>
                            {foreach from=$lot.customers item=customer}
                                <customer inn="{$customer.inn}">
                                    <name>{$customer.name|escape}</name>
                                    <fullName>{$customer.fullName|escape}</fullName>
                                    <factAddress>{$customer.factAddress|escape}</factAddress>
                                    <orgn>{$customer.ogrn}</orgn>
                                    <applicationGuarantee>{$customer.applicationGuarantee/100}</applicationGuarantee>
                                    <contractGuarantee>{$customer.contractGuarantee/100}</contractGuarantee>
                                </customer>
                            {/foreach}
                        </customers>
                    </lot>
                {/foreach}
            </lots>
        </tender>
    {/foreach}
</tenders>