<table>
    <thead>
        <tr>
            <th>Дата публикации</th>
            <th>ИНН Заказчика</th>
            <th>Заказчик</th>
            <th>Наименование закупки</th>
            <th>Вит торгов</th>
            <th>Срок подачи(до)</th>
            <th>Начальная максимальная цена</th>
            <th>Электронная площадка</th>
            <th>Обеспечение заявки</th>
            <th>Обеспечение контракта</th>
        </tr>
    </thead>
    <tbody>
        {foreach from=$customer item=item}
        <tr>
            <td>{$item.pubDate}</td>
            <td>{$item.customer_inn}</td>
            <td>{$item.customer_name}</td>
            <td>{$item.name}</td>
            <td>{$item.type_name}</td>
            <td>{$item.endDate}</td>
            <td>{$item.price}</td>
            <td>{$item.site_name}</td>
            <td>{$item.applicationGuarantee}</td>
            <td>{$item.contractGuarantee}</td>
        </tr>
        {/foreach}
    </tbody>   
</table>