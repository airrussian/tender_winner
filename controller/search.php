<?php

class MTSearchController extends MTBaseController {

    /**
     * Начальная минимальная цена контракта 
     * @var array( min, max ) 
     */
    private $price = array('min' => NULL, 'max' => NULL);

    /**
     * Обеспечение контракта  
     * @var array( min, max )
     */
    private $contractGuarantee = array('min' => NULL, 'max' => NULL);

    /**
     * Способы размещение контракта, массив идентификаторов type
     * @var array ( int, int ... )
     */
    private $type = array();

    /**
     * Заказчики, массив содержащий идентификаторы организаций из БД. 
     * @var array( int, .... )  
     */
    private $customer = array();

    /**
     * Актуальность в днях, если NULL, тогда все
     * @var int
     */
    private $actual = NULL;

    /**
     * @todo продумать типо данных, пока просто список region_id, 
     * Регион компании, более сложная структура данных, может содержать 
     * @var array( int, ... )  
     */
    private $reg = array();

    /**
     * Возраст компании, в месяцах
     * @var int
     */
    private $ageCompony = NULL;

    /**
     * Валюта баланса 
     * @var array(min,max)
     */
    private $balance = array('min' => NULL, 'max' => NULL);

    /**
     * Выручка
     * @var array(min,max)
     */
    private $revenue = array('min' => NULL, 'max' => NULL);

    /**
     * Чистая прибыль
     * @var array(min,max)
     */
    private $profit = array('min' => NULL, 'max' => NULL);

    /**
     * Страница навигации
     * @var int
     */
    private $page = 1;

    /**
     * Количество записей на страницу
     * @var int     
     */
    private $onpage = 10;

    /**
     * Задает порядок сортировок по отдельным столбцам, зависит всё от модели. 
     * Массив переменных, пример $_REQUEST['ord']['price'] = ASC 
     * @var array
     */
    private $order = array();

    /**
     * Возможность применения сортировки по параметрам
     * @var array
     */
    private $order_field = array('reg', 'price', 'cg', 'protocolDate');

    /**
     * Даты 
     * @var array
     */
    public $date = array();

    /**
     * Перед вызовом всегда все параметры поиска парсит из REQUEST
     */
    public function __construct() {
        parent::__construct();

        $this->price = $this->request2interval($this->MTRequest('price'));
        $this->contractGuarantee = $this->request2interval($this->MTRequest('cg'));

        $this->type = $this->Request2Array($this->MTRequest('type'));
        $this->customer = $this->Request2Array($this->MTRequest('customer'));
        $this->actual = (int) $this->MTRequest('actual');

        $this->reg = $this->Request2Array($this->MTRequest('reg'));

        $this->ageCompony = (int) $this->MTRequest('age');

        $this->balance = $this->request2interval($this->MTRequest('balance'));

        $this->revenue = $this->request2interval($this->MTRequest('revenue'));

        $this->profit = $this->request2interval($this->MTRequest('profit'));

        $this->page = (int) $this->MTRequest('page', 1);

        $this->onpage = $this->onpage_value();

        /* Проверка парметров входящих переменных для сортировки по параметрам */
        $order = $this->MTRequest('ord');
        if (!empty($order)) {
            foreach ($order as $key => $ord) {
                if (array_search($key, $this->order_field)) {
                    $this->order[$key] = $ord;
                }
            }
        } else {
            $this->order['protocolDate'] = 'DESC';
        }
    }

    /**
     * Основной экшен, происходит выдача, результата, по заданным параметрам поиска
     */
    public function indexAction() {

        $search = new MTWinnerModel();
        $result = $search->setParam($this->param());

        $type = new typeModel();
        $region = new regionModel();
        $site = new siteModel();
        $org = new organizationModel();

        foreach ($result['item'] as &$item) {
            if (isset($item['region_id'])) {
                $item['region'] = $region->Get($item['region_id']);
                unset($item['region_id']);
            }
            if (isset($item['type_id'])) {
                $item['type'] = $type->Get($item['type_id']);
                unset($item['type_id']);
            }
            if (isset($item['site_id'])) {
                $item['site'] = $site->Get($item['site_id']);
                unset($item['site_id']);
            }

            if (isset($item['organization_id'])) {
                $item['organization'] = $org->Get($item['organization_id']);
                unset($item['organization_id']);
            }
        }

        return json_encode($result);
    }

    /**
     * Примает параметры с $_REQUEST 
     * search[date][load][start] 
     * search[date][load][final]
     * search[date][out][start]
     * search[date][out][final]
     * @return type
     */
    public function exportAction() {

        $search = $this->MTRequest('search');        
        if (isset($search['protocol']['date'])) {
            /* Если задан интервал */
            if (is_array($search['protocol']['date'])) {
                $search['protocol']['date']['min'] = isset($search['protocol']['date']['min']) ? date("Y-m-d H:i:s", strtotime($search['protocol']['date']['min'])) : null;
                $search['protocol']['date']['max'] = isset($search['protocol']['date']['max']) ? date("Y-m-d H:i:s", strtotime($search['protocol']['date']['max'])) : null;
            } else {                                
                $date = isset($search['protocol']['date']) ? $search['protocol']['date'] : null;                
                $search['protocol']['date'] = array();
                $search['protocol']['date']['min'] = date("Y-m-d H:i:s", strtotime($date));
                $search['protocol']['date']['max'] = date("Y-m-d H:i:s", strtotime($date) + 24 * 60 * 60);                
            }
        }
        if (isset($search['protocol']['load'])) {
            if (is_array($search['protocol']['load'])) {
                $search['protocol']['load']['min'] = isset($search['protocol']['load']['min']) ? date("Y-m-d H:i:s", strtotime($search['protocol']['load']['min'])) : null;
                $search['protocol']['load']['max'] = isset($search['protocol']['load']['max']) ? date("Y-m-d H:i:s", strtotime($search['protocol']['load']['max'])) : null;                
            } else {
                $date = isset($search['protocol']['load']) ? $search['protocol']['load'] : null;                
                $search['protocol']['load'] = array();
                $search['protocol']['load']['min'] = date("Y-m-d H:i:s", strtotime($date));
                $search['protocol']['load']['max'] = date("Y-m-d H:i:s", strtotime($date) + 24 * 60 * 60);                
            }
        }
                
        
        $date = $this->MTRequest('date');
        if (!is_null($date)) {
            $time = strtotime($date);
            $search['protocol']['date']['min'] = date("Y-m-d", $time) . ' 00:00:00';            
            $search['protocol']['date']['max'] = date("Y-m-d", $time) . ' 23:59:59';
        }
        

        $one = $this->MTRequest('oneapplication');

        $searchModel = new MTWinnerModel();
        if ($one == 1) {
            $items = $searchModel->GetOneApplication($search);
        } 

        $winners = array();

        $org = new organizationModel();
        foreach ($items as $item) {
            $organization = $org->Get($item['organization_id']);
            $inn = $organization['inn'];
            $winners[$inn][] = array('purchaseNumber' => $item['purchaseNumber'], 'type_id' => $item['type_id'], 'protocolDate' => $item['protocolDate'], 'publishDate' => $item['publishDate']);
        }

        $this->assign('search', $search);
        $this->assign('winners', $winners);

        return $this->show();
    }

    /**
     * Нормализует параметры поиск, и формирует конанический URL.  
     */
    private function normalize_url() {
        
    }

    /**
     * Подготовка данных для передачи модели
     */
    private function param() {
        $result = array();

        if (!is_null($this->actual)) {
            $result['protocolDate']['max'] = date("Y-m-d", time());
            $result['protocolDate']['min'] = date("Y-m-d", time() - $this->actual * 24 * 60 * 60);
        }

        $result['onpage'] = $this->onpage;
        $result['page'] = is_null($this->page) ? 1 : $this->page;
        $result['order'] = $this->order;

        return $result;
    }

    /**
     * Преобразует массив ключей в числовой массив целых числе, ключи отличные от чисел игноруются
     * @param array $array
     * @return array
     */
    private function Request2Array($array = array()) {
        $result = array();
        if (is_array($array)) {
            foreach ($array as $key_id => $val) {
                if (is_numeric($key_id)) {
                    $result[] = $key_id;
                }
            }
            if (!empty($result)) {
                $result = array_unique($result);
                sort($result);
            }
        }
        return $result;
    }

    /**
     * Входные массив разбирает на интервал, во входном массиве должны быть ключи min и max
     * @param array $array
     * @return array
     */
    private function request2interval($array) {
        $result = array('min' => NULL, 'max' => NULL);
        $result['min'] = isset($array['min']) ? (int) $array['min'] : NULL;
        $result['max'] = isset($array['max']) ? (int) $array['max'] : NULL;
        return $result;
    }

}
