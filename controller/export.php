<?php

class MTExportController extends MTBaseController {

    public function indexAction() {
        return "this is no data";
    }

    public function GetTenderList2Action() {
        $purchaseNumbers = $this->MTRequest('numbers');
        if (is_null($purchaseNumbers)) {
            $result = "Укажите номера извещений тендеров";
        }
        $purchaseNumbers = explode(",", $purchaseNumbers);

        $tenderModel = new tenderModel();

        $items = array();

        foreach ($purchaseNumbers as $purchaseNumber) {
            $result = $tenderModel->search($purchaseNumber);
            if (empty($result)) {
                $this->LoadNow($purchaseNumber);
                $result = $tenderModel->search($purchaseNumber);
            }
            if (empty($result)) {
                $items[$purchaseNumber] = NULL;
                continue;
            }            
            
            $items[$purchaseNumber] = array('price' => 0);
            foreach ($result as $lot) {
                $items[$purchaseNumber]['name'] = $lot['name'];
                $items[$purchaseNumber]['lots'][] = array(
                    'price'         =>  (int) $lot['lot_price'] / 100,
                    'bankgarant'    =>  (int) $lot['contractGuarantee'] / 100
                );
                $items[$purchaseNumber]['price'] += isset($lot['lot_price']) ? (int) $lot['lot_price'] / 100 : 0;
            }            
        }
        
        $this->assign('items', $items);
        
        return $this->show();        
    }

    public function GetTenderListAction() {
        $purchaseNumbers = $this->MTRequest('numbers');
        if (is_null($purchaseNumbers)) {
            $result = "Укажите номера извещений тендеров";
        }
        $purchaseNumbers = explode(",", $purchaseNumbers);

        $tenderModel = new tenderModel();
        $result = $tenderModel->GetLotGuarantee($purchaseNumbers);
        
        $items = array();
        
        foreach ($result as $item) {
            $purchaseNumber = $item['purchaseNumber'];
            if (!isset($items[$purchaseNumber])) {
                $items[$purchaseNumber] = array(
                    'name'  =>  $item['name'],
                    'price' =>  0,
                    'contractGuarantee' =>  0,
                    'lots'  =>  array()
                );
            }             
                        
            $items[$purchaseNumber]['contractGuarantee']    += round($item['contractGuarantee'] / 100);
            
            $lot_id = $item['lot_id'];
            if (!isset($items[$purchaseNumber]['lots'][$lot_id])) {
                $items[$purchaseNumber]['price']                += round($item['lot_price'] / 100);
                $items[$purchaseNumber]['lots'][$lot_id] = array(
                    'price'             =>  round($item['lot_price'] / 100),                     
                    'customer'          =>  array(),
                    'contractGuarantee' =>  0
                );                
            }            
            $items[$purchaseNumber]['lots'][$lot_id]['contractGuarantee']   +=  round($item['contractGuarantee'] / 100);
            
            if (!isset($items[$purchaseNumber]['lots'][$lot_id]['customer'])) {
                $items[$purchaseNumber]['lots'][$lot_id]['customer'] = array();
            }            
            $items[$purchaseNumber]['lots'][$lot_id]['customer'][] = round($item['contractGuarantee'] / 100);
        }
                
        $this->assign('items', $items);
        
        return $this->show();        
    }    
    
    public function GetTenderList3Action() {
        $purchaseNumbers = $this->MTRequest('numbers');
        if (is_null($purchaseNumbers)) {
            $result = "Укажите номера извещений тендеров";
        }
        $purchaseNumbers = explode(",", $purchaseNumbers);
        
        $tenderModel = new tenderModel();
        $lotModel = new lotModel();
        $customerModel = new customerModel();
        
        $tenders = $tenderModel->GetByNumber($purchaseNumbers);
        foreach ($tenders as $tender_id => &$tender) {
            $lots = $lotModel->GetLots($tender_id);
            foreach ($lots as &$lot) {
                $lot['customers'] = $customerModel->GetCustomer($lot['id']);
            }
            $tender['lots'] = $lots;
        }        
        
        //var_dump($tenders);
        
        $this->assign('tenders', $tenders);
        
        return $this->show();                
    }    
        
           
}
