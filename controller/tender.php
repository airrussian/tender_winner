<?php

class MTTenderController extends MTBaseController {
    
    public function indexAction() {
        
        $purchaseNumber = $this->MTRequest('purchaseNumber');
        if (!$purchaseNumber) {
            exit("WHERE IS purchaseNumber");
        }
                
        $tenderModel = new tenderModel();
        $items = $tenderModel->GetLotsCustomersTender($purchaseNumber);                
        
        $tender = array();
        
        foreach ($items as $item) {                        
            
            $tender['lots'][$item['lotNumber']]['customers'][$item['customer_inn']] = array(
                'name'  =>  $item['customer_name'],
                'inn'   =>  $item['customer_inn'],
                'applicationGuarantee'  =>  $item['applicationGuarantee'],
                'contractGuarantee'     =>  $item['contractGuarantee']
            );            
            $tender['lots'][$item['lotNumber']]['price'] = $item['lot_price'];
            $tender['lots'][$item['lotNumber']]['name'] = $item['lotObjectInfo'];
            $tender['lots'][$item['lotNumber']]['winner'] = array(
                'inn'   =>  $item['winner_inn'],
                'name'  =>  $item['winner_name']                    
            );
        }
        
        $this->assign('tender', $tender);
        
        
        return $this->show();
        
    }
    
} 