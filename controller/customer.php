<?php

class MTCustomerController extends MTBaseController {
    
    public function indexAction() {
        
        $inn = $this->MTRequest('inn');
        $inn = explode(",", $inn);                
        
        $pubDateFrom = $this->MTRequest('pubdatefrom');
        
        $customer = new customerModel();
        $result = $customer->GetLotsCustomers($inn, $pubDateFrom);
        
        $this->assign('customer', $result);
        
        return $this->show();        
    }
    
    
}