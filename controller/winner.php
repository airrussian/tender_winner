<?php

class MTWinnerController extends MTBaseController {
    
    public function indexAction() {
        
    }
    
    public function showAction() {        
        $date = isset($_REQUEST['date']) ? date("Y-m-d", strtotime($_REQUEST['date'])) : false;                
        if ($date === false) {              // Редирект на специальную страницу с сообщение об ошибке. 
            MTUrlHelper::RedirectError("Для получения информации необходимо Дата в формате YYYY-MM-DD");
        }
        if ($date <> $_REQUEST['date']) {   // Редиректим на правильную ссылку с датой             
            MTUrlHelper::Redirect(MTConfig::baseurl . 'index.php?controller=winner&action=show&date=' . $date );
        }
        
        $m_winners = new winnerModel();
        $winners = $m_winners->GetByDate($date);
        
        $this->view->assign();
        
    }
       
    
}