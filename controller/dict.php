<?php

/**
 * Контроллер отвечает за вывод словарей 
 */

class MTDictController extends MTBaseController {
    
    public function regionAction() {
        $region = dictcacheModel::singleton()->get_raw('region');
        $this->assign('region', $region);
        return $this->show();        
    }
    
    public function typeAction() {
        $type = dictcacheModel::singleton()->get_raw('type');
        $this->assign('type', $type);
        return $this->show();                  
    }
    
    public function countyAction() {
        $county = dictcacheModel::singleton()->get_raw('county');
        $this->assign('county', $county);
        return $this->show();        
    }
    
    public function treeAction() {
        $dict = dictcacheModel::singleton();
        
        $county = $dict->get_raw('county');
        $region = $dict->get_raw('region');
        
        $result = array();
                
        foreach ($county as $cid => $c) {
            $result[$cid]['name'] = $c->name;
            foreach ($region as $rid => $r) {                
                if ($cid == $r->oid) {                    
                    $result[$cid]['region'][$rid] = $r;
                }
            }            
        }
        
        var_dump($result);
    }
    
}