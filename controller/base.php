<?php

abstract class MTBaseController {

    public $view;
    protected $action;
    protected $actionName;
    protected $controllerName;
    public $tmpl;
    
    protected $header_tmpl = array(
        'html'  =>  array(
            'Content-type: text/html; charset=utf-8'
        ),
        'json'  =>  array(
            'Content-type: application/json; charset=utf-8',
            'access-control-allow-origin: *',
        ),
        'xml'   =>  array(
            'Content-type: text/xml; charset=utf-8'
        ),
    );
    
    private $assignData;

    public function __construct() {
        $this->actionName = isset($_REQUEST['action']) ? (string) mb_strtolower($_REQUEST['action']) : 'index';
        $this->action = $this->actionName . "Action";
        $this->controllerName = str_replace(array("MT", "Controller"), "", get_class($this));
        
        $this->tmpl = isset($_REQUEST['tmpl']) ? (string) $_REQUEST['tmpl'] : 'html';
    }

    public function execute() {

        if (method_exists($this, $this->action)) {
            $action = $this->action;
            $result = $this->$action();
        } else {
            
        }

        return $result;
    }

    public function onpage_value( $var = array(10, 20, 50, 100) ) {
        $onpage = isset($_COOKIE['onpage']) ? (int) $_COOKIE['onpage'] : 10;
        $onpage = isset($_REQUEST['onpage']) ? (int) $_REQUEST['onpage'] : $onpage;        
        if ($onpage > max($var)) { $onpage = max($var); }
        if ($onpage < min($var)) { $onpage = min($var); }
        foreach ($var as $val) {
            if ($onpage == $val ) { break; }
        }
        return $onpage;
    }
    
    
    public function MTRequest($name, $default = NULL) {
        return isset($_REQUEST[$name]) ? $_REQUEST[$name] : $default;
    }
    
    public function assign($name, $data) {
        $this->assignData[$name] = $data;
    }


    public function show($tmplName = 'default') {     
        
        $result['headers'] = isset($this->header_tmpl[$this->tmpl]) ? $this->header_tmpl[$this->tmpl] : $this->header_tmpl['html'];
        $result['content'] = "";
        
        if ($this->tmpl == 'xml') {
            $result['content'] .= '<?xml version="1.0" encoding="UTF-8"?>';
        }
        
        if ($this->tmpl == 'json' || $this->tmpl == 'php') {
            $result['content'] = $this->assignData;
            if ($this->tmpl == 'json') {
                $result['content'] = json_encode($result['content']);
            } 
        } else {
        
        /**
         * FIX ME в конфиг
         */
        $savesmartydir = "/mnt/ada0s1/home/air/smarty";
        $basedir = "/mnt/ada0s1/home/air/services/winners";
        
        $tpl = new Smarty();
        
        $tpl->compile_dir = $savesmartydir . '/compile/' . mb_strtolower($this->controllerName) . '/' . $this->actionName;
        if (!file_exists($tpl->compile_dir)) {
            mkdir($tpl->compile_dir, 0777, true);
        } 
        $tpl->cache_dir = $savesmartydir  . '/cache/' . mb_strtolower($this->controllerName) . '/' . $this->actionName;
        if (!file_exists($tpl->cache_dir)) {
            mkdir($tpl->cache_dir, 0777, true);
        } 
        $tpl->template_dir = $basedir . '/view/' . mb_strtolower($this->controllerName) . '/' . $this->actionName;         
        
        /**
         * FIX ME вынести в конфиг
         */
        $tpl->debugging = 1;
        $tpl->error_reporting = E_ALL;
        
        foreach ($this->assignData as $key => $data) {                                       
            $tpl->assign($key, $data);
        }
        
        
        $result['content'].= $tpl->fetch($tmplName . '.' . $this->tmpl . '.tpl');
        }
        
        return $result;
    }
     
}
