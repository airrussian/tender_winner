<?php

class tenderModel extends baseModel {
    
    protected $_tableName = 'tender';
        
    public function Get($id) {
        if (is_array($id)) {            
            $sql = "SELECT * FROM tender WHERE id IN (".  implode(",", $id).")";            
            $query = $this->db->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();                
            $result = $this->IndexArrayOnColumn($result);
        } else {
            $query = $this->db->prepare("SELECT * FROM tender WHERE id = :id");
            $query->execute(array('id' => $id));
            $result = $query->fetch(PDO::FETCH_ASSOC);                        
        }                    
        return $result;
    }

    public function GetByNumber($num) {
        if (is_array($num)) {            
            $sql = "SELECT * FROM tender WHERE purchaseNumber IN ('".  implode("','", $num)."')";            
            $query = $this->db->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();                
            $result = $this->IndexArrayOnColumn($result);
        } else {
            $query = $this->db->prepare("SELECT * FROM tender WHERE purchaseNumber = :number");
            $query->execute(array('number' => $num));
            $row = $query->fetch(PDO::FETCH_ASSOC);
            if (!empty($row)) {
                return $row['id'];
            } else {
                return $this->add(array('purchaseNumber' => $num));
            }
        }            
        return $result;
    }
    
    public function add($tender) {
        $values = "";
        foreach (array_keys($tender) as $nameKey) {
            $values.= "$nameKey = :$nameKey ";
        }
        $query = $this->db->prepare("INSERT INTO tender SET $values");
        $query->execute($tender);
        return $this->db->lastInsertId();
    }

    /**
     * Получается исходный XML закупок 
     * @param type $num
     * @return type
     */    
    public function GetNotificationXML($num) {

        $BASE_PATH = '/mnt/ada0s1/home/admin/zakupki';
        $db = $BASE_PATH . '/db/notifications.sdb';
        $dbh = new PDO('sqlite:' . $db);

        $sql = "SELECT * from notifications WHERE purchaseNumber='$num'";
        $sth = $dbh->prepare($sql);
        $sth->execute();
        $data = array();
        $zipname = '';
        if ($r = $sth->fetch(PDO::FETCH_ASSOC)) {
            $zipname = $r['fname'];
            $time = $r['time'];
            $xmlname = 'fcsNotification' . $r['placingWay'] . '_' . $r['purchaseNumber'] . '_' . $r['internal_id'] . '.xml';
            if (!file_exists($BASE_PATH . '/' . $zipname)) {
                if (preg_match('/currMonth/', $zipname)) {
                    $zipname = str_replace('currMonth', 'prevMonth', $zipname);
                }
            }
        }
        $sth->closeCursor();
        $dbh = null;
        if (empty($zipname) || !file_exists($BASE_PATH . '/' . $zipname)) {
            $result = false;
        } else {
            $fdata = file_get_contents('zip://' . $BASE_PATH . '/' . $zipname . '#' . $xmlname);
            $fdata = preg_replace('/<cryptoSigns[^>]*?>.*?<\/cryptoSigns>\s*/ims', '', $fdata);
            $fdata = preg_replace('/<signature[^>]*?>.*?<\/signature>\s*/ims', '', $fdata);
            if (preg_match('/<ns2:export/', $fdata))
                $fdata = preg_replace('/<[\/]?ns2:[a-z]*?notification[^>]*?>\s*/ims', '', $fdata);
            $result = $fdata;
        }
        return $result;
    }
    
    /**
     * FIXME времянка
     * @param type $purchaseNumber
     * @return type
     */
    public function search($purchaseNumber) {        
        $sql = "SELECT purchaseNumber, name, lot.id as lot_id, lot.price as lot_price, contractGuarantee FROM tender, lot, customer WHERE tender.id = lot.tender_id AND lot.id = customer.lot_id AND tender.purchaseNumber LIKE :purchaseNumber";
        $query = $this->db->prepare($sql);
        $query->execute(array("purchaseNumber" => $purchaseNumber));
        return $query->fetchAll();        
    }
    
    public function GetLotGuarantee($purchaseNumbers) {
        $cnt = count($purchaseNumbers);
        if ($cnt > 0) {
            $in = array();
            for ($i = 0; $i<$cnt; $i++) { $in[] = '?'; }
            $in = implode(",", $in);
        } else {
            return false;
        }        
        
        $sql = "SELECT purchaseNumber, name, lot.id as lot_id, lot.price as lot_price, contractGuarantee FROM tender, lot, customer "
                . "WHERE tender.id = lot.tender_id AND lot.id = customer.lot_id AND "
                . "tender.purchaseNumber IN ($in) ";
        $query = $this->db->prepare($sql);
        $query->execute($purchaseNumbers);
        return $query->fetchAll();        
    }
    
    public function GetListId($purchaseNumbers) {
        $sql = "SELECT id, purchaseNumber FROM tender WHERE purchaseNumber IN ('".implode("','", $purchaseNumbers)."')";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }
    
    public function GetLotsCustomersTender($purchaseNumber) {
        $sql = "SELECT 
                    tender.id as id, purchaseNumber, tender.name as tender_name, lot.lotNumber as lotNumber, lot.lotObjectInfo, lot.price as lot_price, 
                    cust.inn as customer_inn, cust.name as customer_name, customer.applicationGuarantee, customer.contractGuarantee,
                    apps.price, winner.inn as winner_inn, winner.name as winner_name
                FROM tender, lot 
                    LEFT JOIN customer ON lot.id = customer.lot_id 
                    LEFT JOIN organization as cust ON cust.id = customer.organization_id 
                    LEFT JOIN apps ON apps.lot_id = lot.id AND rating = 1
                    LEFT JOIN organization as winner ON winner.id = apps.organization_id
                WHERE lot.tender_id = tender.id AND tender.purchaseNumber LIKE ('$purchaseNumber')";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }
    
    public function GetLotsCustomers(array $purchaseNumbers) {
        $sql = "SELECT * FROM tender, lot, customer WHERE purchaseNumber IN ('".  implode("','", $purchaseNumbers)."') 
                    AND lot.tender_id = tender.id AND customer.lot_id = lot.id  
                    ORDER BY tender.id ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

}
