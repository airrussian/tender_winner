<?php

class lotModel extends baseModel {
    
     protected $_tableName = 'lot';
     
     
     public function GetLots($tender_id) {
         $sql = "SELECT * FROM {$this->_tableName} WHERE tender_id = :tender_id";
         $query = $this->db->prepare($sql);
         $query->execute(array('tender_id'=>$tender_id));
         return $query->fetchAll();
     }
     
     /**
      * Возвращает массив всех лотов, по тендерам указанных в tender_ids
      * @param array $tender_ids
      */
     public function GetLotsTender($tender_ids) {
         if (!is_array($tender_ids) || empty($tender_ids)) {
             return false;
         }         
         $return = array();         
         $sql = "SELECT * FROM {$this->_tableName} WHERE tender_id IN (" . implode(",", $tender_ids) . ")";
         $query = $this->db->prepare($sql);
         $query->execute();
         
         $result = $query->fetchAll();
         
         foreach ($result as $lots) {
             $return[$lots['tender_id']][$lots['lotNumber']] = $lots;
         }
         
         return $return;
         
     }
     
}