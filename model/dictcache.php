<?php

class dictcacheModel extends baseModel {
    
    private $memcache;
    
    /**
     * Хранит данные кэша
     * @var type 
     */    
    private $data;
    
    /**
     * Возвращается таблицу кэша
     * @param type $name
     * @return type
     */
    public function get_raw($name = null) {
        if (empty($name)) {
            return $this->data;
        } else {
            return $this->data[$name];
        }
    }    
    
    /**
     * Возвращает все данные хранящиеся в кэше
     * @return array
     */    
    public function get_data() {
        return $this->data;
    }    
    
    private static $instance;
    private static $canInstanciate = false;
    
    public static function singleton() {        
        if (!isset(self::$instance)) {
            self::$canInstanciate = true;
            self::$instance = new self;
            self::$canInstanciate = false;
        }        
        return self::$instance;
    }

    public function __construct() {
        if (!self::$canInstanciate) {
            trigger_error('Unable to instantiate, this is singleton, run ass CLASS::singleton', E_USER_ERROR);
        }
        
        parent::__construct();
        
        try {
            $this->memcache = new Memcache();
            /* FIXME в конфиг */
            $this->memcache->connect("127.0.0.1", 11611);
            $this->init();
        } catch ( Exception $e ) {
            echo "Fail: " . $e->getMessage() . PHP_EOL;
        }
    }

    private function init() {
        $md5_key = md5($_SERVER['SERVER_NAME'] . __FILE__ . 'data');

        if ($this->memcache) {
            $this->data = $this->memcache->get($md5_key);
        }

        if (empty($this->data)) {
            $this->_load();            

            if ($this->memcache) {
                if (!$this->memcache->replace($md5_key, $this->data, 0, $this->RndTimeExpire())) {
                    $this->memcache->set($md5_key, $this->data, 0, $this->RndTimeExpire());
                }
            }
        }

        if (empty($this->data)) {
            exit('FATAL, no data');
        }
    }
    
    /**
     * Содержит описание сущностей которые должны быть закешированы
     */    
    private function _load() {
        
        // Кэшируем регионы
        $regionModel = new regionModel();
        $this->data['region'] = $regionModel->GetAll();        
        
        // Кэшируем типы
        $typeModel = new typeModel();
        $this->data['type'] = $typeModel->GetAll();
                
        // Кэшируем округа
        $countyModel = new countyModel();
        $this->data['county'] = $countyModel->GetAll();
    }
    
    
    /**
     * 
     * @return type
     */    
    private function RndTimeExpire() {
        $time = 60*60;
        return mt_rand(0.9*$time, 1.1*$time);
    }    

}
