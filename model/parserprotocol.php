<?php

class parserprotocolModel extends baseModel {
    
    public function getParserType($type) {
        $query = $this->db->prepare("SELECT name FROM parser_protocol WHERE type = :type LIMIT 1");
        $query->execute(array('type' => $type));
        $row = $query->fetch(PDO::FETCH_ASSOC);
        if (!isset($row['name']) || empty($row['name']) || is_null($row['name'])) {
            return false;
        }
            return $row['name'];
        
    }
    
}