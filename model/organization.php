<?php

class organizationModel extends baseModel {

    protected $_tableName = 'organization';

    public function GetId($arr) {

        $arr['inn'] = trim(preg_replace("~\D~si", "", $arr['inn']));

        $query = $this->db->prepare("SELECT * FROM {$this->_tableName} WHERE inn LIKE :inn LIMIT 1");
        $query->execute(array('inn' => $arr['inn']));
        $row = $query->fetch(PDO::FETCH_ASSOC);

        if (!empty($row) && isset($row['id'])) {
            return $row['id'];
        } else {
            $query = $this->db->prepare("INSERT INTO {$this->_tableName}(inn, kpp, name, postAddress) VALUES (:inn, :kpp, :name, :postAddress)");
            if (!$query->execute($arr)) {
                print_r($query->errorInfo());
                return false;
            }
            return $this->db->lastInsertId();
        }
    }

    public function Get($org_id) {
        $query = $this->db->prepare("SELECT * FROM {$this->_tableName} WHERE id = ?");
        $query->execute(array($org_id));
        $row = $query->fetch(PDO::FETCH_ASSOC);
        if (!$row) {
            return false;
        }
        return $row;
    }

    public function GetOrgListByRegNum($start_id, $limit = 1000) {
        $query = $this->db->prepare("SELECT * FROM {$this->_tableName} WHERE regnum IS NOT NULL AND inn IS NULL AND id>:id LIMIT :limit");
        $query->bindValue(':id', (int) $start_id, PDO::PARAM_INT);
        $query->bindValue(':limit', (int) $limit, PDO::PARAM_INT);
        $query->execute();
        return $query->fetchAll();
    }

    public function update($id, $data) {

        $set = array();
        foreach ($data as $fieldName => $fieldValue) {
            if (!is_null($fieldValue)) {
                $set[] = "$fieldName = :$fieldName";
            }
        }

        $sql = "UPDATE IGNORE {$this->_tableName} SET " . implode(",", $set) . " WHERE id = $id";
        
        $query = $this->db->prepare($sql);
        return $query->execute($data);
    }

}
