<?php

class siteModel extends baseModel {
    
    public function GetId($code) {
        if ($code == "") { return false; }
        
        $sql = "SELECT id FROM site WHERE code LIKE :code";
        $query = $this->db->prepare($sql);
        $query->execute(array('code' => $code));
        $result = $query->fetch(PDO::FETCH_NUM);        
        if (!$result) {
            print_r($query->errorInfo()); 
            return false;            
        }
        return $result[0];        
    }
    
    public function Get($site_id) {
        
        $sql = "SELECT * FROM site WHERE id = ?";
        $query = $this->db->prepare($sql);
        $query->execute(array($site_id));
        $result = $query->fetch(PDO::FETCH_ASSOC);
        if (!$result) {
            print_r($query->errorInfo()); 
            return false;            
        }
        return $result;        
    }
    
    public function GetAll() {
        $result = array();
        $sql = "SELECT * FROM site ORDER BY id ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_GROUP | PDO::FETCH_UNIQUE | PDO::FETCH_CLASS);
        return $result;
    }
    
}