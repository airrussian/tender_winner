<?php

class typeModel extends baseModel {

    public function dict($name, $fullname, $parse_id) {

        try {
            $sql = "SELECT * FROM type_dict WHERE name LIKE ? AND parse_id = ? LIMIT 1";
            $q = $this->db->prepare($sql);
            $q->execute(array($name, $parse_id));
            $r = $q->fetch(PDO::FETCH_ASSOC);
            if (!$r) {    // Если нет добавляем 
                $sql = "INSERT INTO type_dict(`name`, `fullname`, `parse_id`) VALUES(?, ?, ?)";
                $q = $this->db->prepare($sql);
                if (!$q->execute(array($name, $fullname, $parse_id))) {
                    print_r($q->errorInfo());
                    return false;
                }
                $id = $this->db->lastInsertId();
                $r = array('id' => $id, 'name' => $name, 'type_id' => NULL, 'parse_id' => $parse_id);
            }
        } catch (PDOException $e) {
            print_r("FAIL IN PDO");
            return false;
        }
        return $r;
    }

    public function Get($type_id) {
        $sql = "SELECT * FROM type WHERE id = ?";
        $q = $this->db->prepare($sql);
        $q->execute(array($type_id));
        $r = $q->fetch(PDO::FETCH_ASSOC);
        if (!$r) {
            print_r($q->errorInfo());
            return false;
        }
        return $r;
    }
    
    public function GetAll() {
        $result = array();
        $sql = "SELECT * FROM type ORDER BY id ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_GROUP | PDO::FETCH_UNIQUE | PDO::FETCH_CLASS);
        return $result;
    }

}
