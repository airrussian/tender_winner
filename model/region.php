<?php

class regionModel extends baseModel {

    public function dict($name, $parse_id) {

        // Поиск по названию в таблице сопостовления регионов
        $sql = "SELECT * FROM region_dict WHERE name LIKE ? AND parse_id = ? LIMIT 1";
        $q = $this->db->prepare($sql);
        $q->execute(array($name, $parse_id));
        $r = $q->fetch(PDO::FETCH_ASSOC);
        if (!$r) {    // Если нет добавляем 
            $sql = "INSERT INTO region_dict(`name`, `parse_id`) VALUES(?, ?)";
            $q = $this->db->prepare($sql);
            $q->execute(array($name, $parse_id));
        }
        return $r;
    }

    public function Get($region_id) {
        $sql = "SELECT * FROM region WHERE id = ?";
        $q = $this->db->prepare($sql);
        $q->execute(array($region_id));
        $r = $q->fetch(PDO::FETCH_ASSOC);
        if (!$r) {
            print_r($query->errorInfo());
            return false;
        }
        return $r;
    }

    public function GetAll() {
        $result =array();
        $sql = "SELECT * FROM region ORDER BY id ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_GROUP | PDO::FETCH_UNIQUE | PDO::FETCH_CLASS);        
        return $result;
    }

}
