<?php

class winnerModel extends baseModel {

    public function GetByDate($date) {

        $time = strtotime($date);

        $query = $this->db->prepare("
            SELECT organization.inn, GROUP_CONCAT( tender.purchaseNumber ) as tender_ids FROM protocol
            LEFT JOIN application ON application.protocol_id = protocol.id 
            LEFT JOIN organization ON application.organization_id = organization.id
            LEFT JOIN tender ON tender.id = protocol.tender_id
            WHERE protocolDate >= :startdate AND protocolDate < :finaldate AND rating=1 
            GROUP BY organization.inn;
        ");

        $query->execute(array("startdate" => date("Y-m-d H:i:s", $time), 'finaldate' => date("Y-m-d H:i:s", $time + 24 * 60 * 60)));
        $r = $query->fetchAll();

        return $r;
    }

    public function GetByDateLoad($date) {
        $time = strtotime($date);

        $query = $this->db->prepare("
            SELECT organization.inn, tender.purchaseNumber, protocol.protocolDate, protocol.signDate, protocol.publishDate FROM protocol
            LEFT JOIN application ON application.protocol_id = protocol.id 
            LEFT JOIN organization ON application.organization_id = organization.id
            LEFT JOIN tender ON tender.id = protocol.tender_id
            WHERE loadDate >= :startdate AND loadDate < :finaldate AND rating=1");

        $query->execute(array("startdate" => date("Y-m-d H:i:s", $time), 'finaldate' => date("Y-m-d H:i:s", $time + 24 * 60 * 60)));
        $r = $query->fetchAll();

        return $r;
    }

    public function GetByPurchaseNumber($number) {

        $query = $this->db->prepare("
                SELECT * FROM tender 
                    LEFT JOIN protocol ON protocol.tender_id = tender.id
                    LEFT JOIN application ON application.protocol_id = protocol.id 
                    LEFT JOIN organization ON organization.id = application.organization_id 
                WHERE purchaseNumber LIKE :number AND application.rating = 1");

        $query->execute(array("number" => $number));
        $r = $query->fetchAll();

        return $r;
    }

}

class MTWinnerModel extends MTBaseModel {

    /**
     * Возвращает организацию победителя
     * @param type $number
     * @return type
     */
    public function GetByPurchaseNumber($number) {

        $query = $this->db->prepare("
                SELECT organization.* FROM tender 
                    LEFT JOIN protocol ON protocol.tender_id = tender.id
                    LEFT JOIN application ON application.protocol_id = protocol.id 
                    LEFT JOIN organization ON organization.id = application.organization_id 
                WHERE purchaseNumber LIKE :number AND application.rating = 1");

        $query->execute(array("number" => $number));
        $r = $query->fetchAll();

        return $r;
    }

    public function setParam($param) {

        $sql = "SELECT SQL_CALC_FOUND_ROWS protocol_id, organization_id, protocolNumber, protocolDate, tender.* 
            FROM application, protocol, tender 
            WHERE protocol.id = application.protocol_id AND protocol.tender_id = tender.id ";

        if (isset($param['oneapplication'])) {
            $sql.= " AND (application.rating IS NULL AND application.admitted = 1) ";
        } else {
            $sql.= " AND ((application.rating IS NULL AND application.admitted = 1) OR (application.rating=1 AND application.admitted = 1)) ";
        }

        if (isset($param['protocolDate']['min'])) {
            $sql.= " AND protocolDate >= '{$param['protocolDate']['min']}' ";
        }

        if (isset($param['protocolDate']['max'])) {
            $sql.= " AND protocolDate <= '{$param['protocolDate']['max']}' ";
        }

        if (isset($param['order'])) {
            $sql.= " ORDER BY ";
            $fields = array();
            foreach ($param['order'] as $field => $o) {
                $o = strtoupper($o);
                if (($o == 'ASC') || ($o == 'A')) {
                    $o = 'ASC';
                }
                if (($o == 'DESC') || ($o == 'D')) {
                    $o = 'DESC';
                }
                $fields[] = $field . " " . $o;
            }
            $sql.= implode(", ", $fields);
        }

        if (isset($param['page'])) {
            $start = ($param['page'] - 1) * $param['onpage'];
        }
        if (!isset($param['all'])) {
            $sql.= " LIMIT :offset, :limit ";
        }

        //var_dump($sql);

        $query = $this->db->prepare($sql);

        if (isset($start)) {
            $query->bindValue(':offset', (int) $start, PDO::PARAM_INT);
        }
        if (isset($param['onpage'])) {
            $query->bindValue(':limit', (int) $param['onpage'], PDO::PARAM_INT);
        }
        $query->execute();

        $count_query = $this->db->query("SELECT FOUND_ROWS()");
        $result['count'] = (int) $count_query->fetchColumn();
        $result['item'] = $query->fetchAll();

        return $result;
    }

    public function search() {
        
    }

    public function GetOneApplication($param) {
        
        $sql = "SELECT protocol.id, protocol.protocolDate, protocol.publishDate, tender.purchaseNumber, tender.type_id, application.organization_id, COUNT(protocol_id) as C
            FROM protocol, application, tender 
            WHERE protocol.id = application.protocol_id AND protocol.tender_id = tender.id ";
        
        if (isset($param['protocol']['date']) && is_array($param['protocol']['date'])) {
            $sql .= " AND protocol.protocolDate >= :protocolDateMin AND protocol.protocolDate <= :protocolDateMax ";
        }

        if (isset($param['protocol']['load']) && (is_array($param['protocol']['load']))) {
            $sql .= " AND protocol.loadDate >= :protocolDateLoadMin AND protocol.loadDate <= :protocolDateLoadMax ";
        }

        $sql .= " AND application.admitted = 1 GROUP BY protocol_id HAVING c = 1 ";                                        
        $query = $this->db->prepare($sql);
        

        if (isset($param['protocol']['date']) && (is_array($param['protocol']['date']))) {
            $query->bindValue(':protocolDateMin', $param['protocol']['date']['min'], PDO::PARAM_STR);
            $query->bindValue(':protocolDateMax', $param['protocol']['date']['max'], PDO::PARAM_STR);
        }

        if (isset($param['protocol']['load']) && (is_array($param['protocol']['load']))) {
            $query->bindValue(':protocolDateLoadMin', $param['protocol']['load']['min'], PDO::PARAM_STR);
            $query->bindValue(':protocolDateLoadMax', $param['protocol']['load']['max'], PDO::PARAM_STR);
        }

        $query->execute();

        $result = $query->fetchAll();

        return $result;
    }

}
