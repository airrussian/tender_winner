<?php

class countyModel extends baseModel {
    
    public function GetAll() {
        $result = array();
        $sql = "SELECT * FROM county ORDER BY id ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_GROUP | PDO::FETCH_UNIQUE | PDO::FETCH_CLASS);
        return $result;
    }
    
}