<?php

class protocolModel extends baseModel {

    public function add($protocol) {
        $q = $this->db->prepare("SELECT id FROM protocol WHERE tender_id = ? AND internal_id = ?");
        $q->execute(array($protocol['tender_id'], $protocol['internal_id']));
        $r = $q->fetch();
        if ($r['id'] > 0) {
            return $r['id'];
        }
        return $this->insert('protocol', $protocol);
    }

    public function getTendersByDate($date) {
        $time = strtotime($date);
        $query = $this->db->prepare("SELECT tender_id FROM protocol WHERE protocolDate >= :startdate AND protocolDate < :finaldate");
        $query->execute(array("startdate" => date("Y-m-d H:i:s", $time), 'finaldate' => date("Y-m-d H:i:s", $time + 24 * 60 * 60)));
        $result = $query->fetchAll(PDO::FETCH_COLUMN);

        return $result;
    }

    public function getProtocolByDate($date) {
        $time = strtotime($date);
        $query = $this->db->prepare("SELECT id FROM protocol WHERE protocolDate >= :startdate AND protocolDate < :finaldate");
        $query->execute(array("startdate" => date("Y-m-d H:i:s", $time), 'finaldate' => date("Y-m-d H:i:s", $time + 24 * 60 * 60)));
        $result = $query->fetchAll(PDO::FETCH_COLUMN);

        return $result;
    }
    
    public function GetPurchaseNumber($id) {
        
        $query = $this->db->prepare("SELECT tender.purchaseNumber FROM tender, protocol WHERE protocol.id = $id AND protocol.tender_id = tender.id");
        $query->execute();
        $result = $query->fetch(PDO::FETCH_ASSOC);        
        return isset($result['purchaseNumber']) ? $result['purchaseNumber'] : false;
    }

    /**
     * Времянка для получения от макса данных
     */
    public function getByNumber($number) {
        $BASE_PATH = '/mnt/ada0s1/home/admin/zakupki';
        $db = $BASE_PATH . '/db/protocols.sdb';
        $dbh = new PDO('sqlite:' . $db);

        $sql = "SELECT * from data WHERE purchaseNumber='$number' order by internal_id asc";
        $sth = $dbh->prepare($sql);
        $sth->execute();
        $zipname = '';
        $xml = '';
        while ($r = $sth->fetch(PDO::FETCH_ASSOC)) {
            $zipname = $r['fname'];
            $time = $r['time'];
            $xmlname = 'fcsProtocol' . $r['placingWay'] . '_' . $r['purchaseNumber'] . '_' . $r['internal_id'] . '.xml';
            if (!file_exists($BASE_PATH . '/' . $zipname)) {
                if (preg_match('/currMonth/', $zipname)) {
                    $zipname = str_replace('currMonth', 'prevMonth', $zipname);
                }
            }
            if (empty($zipname) || !file_exists($BASE_PATH . '/' . $zipname))
                continue;
            $fdata = file_get_contents('zip://' . $BASE_PATH . '/' . $zipname . '#' . $xmlname);
            $fdata = preg_replace('/<cryptoSigns[^>]*?>.*?<\/cryptoSigns>\s*/ims', '', $fdata);
            $fdata = preg_replace('/<signature[^>]*?>.*?<\/signature>\s*/ims', '', $fdata);
            if (preg_match('/<ns2:export/', $fdata))
                $fdata = preg_replace('/<[\/]?ns2:[a-z]*?protocol[^>]*?>\s*/ims', '', $fdata);
            if (preg_match('/<ns2:export[^>]*?>\s*(.*?)\s*<\/ns2:export>/ims', $fdata, $m)) {
                $xml.="<protocol>\n<zip>$zipname</zip>\n<xmlname>$xmlname</xmlname>\n" . $m[1] . "</protocol>\n";
            }
        }
        $sth->closeCursor();
        $dbh = null;
        $xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<protocols>\n$xml</protocols>\n";
        return $xml;
    }

}
