<?php

class appsModel extends baseModel {
    
    protected $_tableName = 'apps';
    
    public function Update($data) {
        $fields = array();
        
        $update = array();
        $primaryfields = array('lot_id', 'protocol_id', 'journalNumber');
        foreach ($data as $fieldName => $fieldValue) {
            $n = array_search($fieldName, $primaryfields);
            if (!is_bool($n)) {
                $update[$fieldName] = "" . $fieldName . " = :" . $fieldName;
            } else {
                $fields[$fieldName] = $fieldValue;
            }
        }
        
        $sql = "UPDATE {$this->_tableName} SET " . $this->setPdo($fields) . " WHERE " . implode(" AND ", $update);
                
        $query = $this->db->prepare($sql);
        if (!$query->execute($data)) {
            print_r($query->errorInfo());
            return false;
        }
        
        return $data;
    }
    
}