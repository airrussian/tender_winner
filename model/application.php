<?php

class applicationModel extends baseModel {
    
    public function add($application) {
        $update = array();
        if (isset($application['rating'])) {
            $update[] = "rating = '{$application['rating']}'";
        }
        if (isset($application['admitted'])) {
            $update[] = "admitted = '{$application['admitted']}'";
        }        
        if (!empty($update)) {
            $update = implode(",", $update);
        } else {
            $update = false;
        }
        
        
        $this->insert('application', $application, $update);
    }
    
    
}