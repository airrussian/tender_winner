<?php

class baseModel {
    
    protected $db; 

    public function __construct() {
        $this->db = &$GLOBALS['DB'];
    }
 
    public function insert($tableName, $param, $update=false) {
        $values = array(); foreach (array_keys($param) as $nameKey) { $values[]= "$nameKey = :$nameKey"; }   
        
        $values = implode(", ", $values);
        
        try {
            $sql = "INSERT INTO $tableName SET $values";
            if ($update != false) {
                $sql.= " ON DUPLICATE KEY UPDATE $update";                
            }                        
            $query = $this->db->prepare($sql);        
            if (!$query->execute($param)) {
                print_r($query->errorInfo()); 
                return false;
            }             
        } catch (PDOException $e) {
            print_r("FAIL IN PDO");
            return false;
        }
        return $this->db->lastInsertId();        
    }    
    
    public function setPdo($fields) {         
        $set = array();
        foreach ($fields as $key => $value) {
            $set[] = "$key = :$key";
        }
        return implode(", ", $set);
    }
    
    public function Load($WHERE, $VALUES) {
        
        $sql = "SELECT * FROM {$this->_tableName} WHERE $WHERE LIMIT 1";        
        $query = $this->db->prepare($sql);
        $query->execute($VALUES);
        $result = $query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }    
    
    public function Save($data) {
        $fields = $data;
        if (isset($fields['id'])) {
            unset($fields['id']);
            $sql = "UPDATE {$this->_tableName} SET " . $this->setPdo($fields) . " WHERE id = :id";
        } else {            
            $sql = "INSERT INTO {$this->_tableName} SET " . $this->setPdo($fields);
        }
             
        $query = $this->db->prepare($sql);
        if (!$query->execute($data)) {
            print_r($query->errorInfo());
            return false;
        }
        if ($id = $this->db->lastInsertId()) {        
            $data['id'] = $id;
        }
        return $data;
    }
    
    
    /**
     * Численный список, переобразует в список
     * @param type $arr
     * @param type $field
     */
    public function IndexArrayOnColumn($list, $fieldName = 'id') {
        $return = array();
        
        if (!is_array($list) || empty($list)) {
            return false;
        }
        foreach ($list as $item) {
            if (isset($item[$fieldName])) {     
                $return[$item[$fieldName]] = $item;
            }
        }
        return $return;
    }
    
    
}

class MTBaseModel {
    
    protected $db; 
    
    public function __construct() {
        $this->db = &$GLOBALS['DB'];
    }
    
}