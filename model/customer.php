<?php

class customerModel extends baseModel {
    
    protected $_tableName = "customer";
    
    
    public function GetCustomer($lot_id) {
        $sql = "SELECT * FROM customer, organization WHERE customer.lot_id = :lot_id AND customer.organization_id = organization.id";
        $query = $this->db->prepare($sql);
        $query->Execute(array('lot_id' => $lot_id));
        return $query->fetchAll();
    }
    
    /**
     * Возращает список всех лотов для заказчиков с ИНН указанных в INN, начиная с указанного ID       
     * 
     */    
    public function GetLotsCustomers($inn, $pubDate = false) {
        if (is_array($inn)) {
            $inn_in = "'" . implode("', '", $inn) . "'";
        } else {
            $inn_in = "'" . $inn . "'";
        }
        
        
        $sql = "SELECT lot.id, tender.pubDate, tender.purchaseNumber, organization.inn as customer_inn, organization.name as customer_name, tender.name, `type`.name as type_name, tender.endDate, lot.price, site.name as site_name, customer.applicationGuarantee, customer.contractGuarantee
                FROM lot, tender, customer, organization, `type`, `site`
                WHERE lot.id = customer.lot_id AND tender.id = lot.tender_id AND customer.organization_id = organization.id AND tender.type_id = `type`.id AND tender.site_id = site.id
                AND organization.inn IN ($inn_in)";
        if ($pubDate) {
            $pubDate = date("Y-m-d H:i:s", strtotime($pubDate));
            $sql.= " AND tender.pubDate > '$pubDate' ";
        }
                    
            $sql.= " ORDER BY tender.id ASC";
        
        $query = $this->db->prepare($sql);
        $query->execute();
        
        $result = $query->fetchAll();
        
        return $result;
        
        
    }
}