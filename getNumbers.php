<?php

require_once dirname(__FILE__) . "/main.php";

$purchaseNumbers = isset($_REQUEST['numbers']) ? $_REQUEST['numbers'] : false;

$mttender = file_get_contents("http://multitender.ru/tenders/?action=view&get=1&numbers=".$purchaseNumbers);
$mttender = json_decode($mttender, true);
//var_dump($mttender); exit();

$tmpl = isset($_REQUEST['tmpl']) ? $_REQUEST['tmpl'] : 'xml';

$purchaseNumbers = explode(",", $purchaseNumbers);

$tender = new tenderModel();

$result = array();

foreach ($purchaseNumbers as $purchaseNumber) {        
    $purchaseNumber = trim($purchaseNumber);
    $t = $tender->GetNotificationXML($purchaseNumber);    
    $xml = simplexml_load_string($t);    
    
    if (is_object($xml)) {
        if (isset($xml->lots)) {
            $price = reset($xml->lots->lot->maxPrice);
        } else {
            if (isset($xml->lot->maxPrice)) {
                $price = reset($xml->lot->maxPrice);
            } else {
                $price = NULL;
            }
        }
        $result[$purchaseNumber] = array('name' => reset($xml->purchaseObjectInfo), 'price' => $price);
    } else {        
        if (isset($mttender[$purchaseNumber])) {
            $result[$purchaseNumber] = $mttender[$purchaseNumber];
        } else {
            $result[$purchaseNumber] = array();
        }
    }            
    $xml = null;
}

if ($tmpl == 'xml') {
    header("Content-type: text/xml; charset=utf-8");
    echo '<?xml version="1.0" encoding="UTF-8"?>';
    echo '<tenders>';   
    foreach ($result as $purchaseNumber => $row) {
        echo '<tender purchaseNumber="'.trim($purchaseNumber).'">';
        if (!empty($row)) {
            echo '<name>'.$row['name'].'</name>';
            echo '<price>'.$row['price'].'</price>';
        }
        echo "</tender>";
    }
    echo "</tenders>";
} else {
    if ($tmpl == 'json') {
        header('Content-type: application/json; charset=utf-8');
        header("access-control-allow-origin: *"); 
        echo(json_encode($result));
    } 
    if ($tmpl == 'php') {
        header("Content-type: text/html; charset=utf-8");
        print_r($result);
    }
}


