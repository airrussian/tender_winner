<?php

require_once dirname(__FILE__) . '/main.php';

$date = isset($_REQUEST['date']) ? $_REQUEST['date'] : $argv[1];
if (!$date) {
    exit("WHERE IS DATE?");
}

header("Content-type: text/xml; charset=utf-8");

if (!file_exists(dirname(__FILE__) . '/cache/one/' . $date . '.xml') || isset($_REQUEST['nocache'])) {
    
    $content = file_get_contents("http://win.multitender.ru/show.php?date=$date");
    $result = simplexml_load_string($content,NULL, LIBXML_NOCDATA|LIBXML_COMPACT|LIBXML_PARSEHUGE);
   
    $arr = json_decode(@json_encode($result),1);
    $winners = $arr['winner'];
    
    $winners = oneapplication($winners); 
    
    $result = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<winners date=\"$date\">\n";
    foreach ($winners as $inn => $purchaseNumbers) {
        $result.= "\t<winner inn=\"{$inn}\">\n";        
        foreach ($purchaseNumbers as $purchaseNumber) {            
            $result.= "\t\t<purchaseNumber>" . $purchaseNumber . "</purchaseNumber>\n";
        }
        $result.= "\t</winner>\n";
    }
    $result.= "</winners>\n";
    file_put_contents(dirname(__FILE__) . '/cache/one/' . $date . '.xml', $result);
} else {
    $result = file_get_contents(dirname(__FILE__) . '/cache/one/' . $date . '.xml');
}
echo $result;

function oneapplication($winners) {
    $return = array();
    foreach ($winners as $winner) {        
        $purchaseNumbers = is_array($winner['purchaseNumber']) ? $winner['purchaseNumber'] : array($winner['purchaseNumber']);
        foreach ($purchaseNumbers as $purchaseNumber) {           
             if (test($purchaseNumber)) {                 
                 $return[$winner['@attributes']['inn']][] = $purchaseNumber;
             }
        }        
    }    
    
    return $return;
}

function test($purchaseNumber) {
    $parser = new protocolParser();
    $data = $parser->parse($purchaseNumber);
    $app = 0;    
    foreach ($data['protocol'] as $protocol) {        
        if (isset($protocol['application'])) {
            $app = count($protocol['application']);
        }
    }    
    return $app == 1;
}