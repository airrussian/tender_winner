<?php

require_once dirname(__FILE__) . '/main.php';

$purchaseNumber = isset($argv[1]) ? $argv[1] : NULL;

if (!is_null($purchaseNumber)) {
    echo "parse = $purchaseNumber \n";
    $loader = new winnerLoader();
    $loader->run($purchaseNumber);        
}

